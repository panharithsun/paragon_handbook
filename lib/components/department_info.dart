import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DepartmentInformation extends StatelessWidget {

  final String imgUrl;
  final String number;
  final String departmentName;
  final String departmentQuestion;
  final String description;
  final double height;
  final double top;
  final Function function;
  final double topPadding;
  final double leftPadding;
  final double scrollHeight;

  const DepartmentInformation({
    Key key, this.number, this.departmentName, this.departmentQuestion, this.description, this.imgUrl, this.height, this.top = 115, this.function, this.topPadding = 50.0, this.leftPadding = 24.0, this.scrollHeight = 0.45
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          child: Image.asset(
            imgUrl,
            fit: BoxFit.fitHeight,
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
            child: Container(
              color: Color(0xff000000).withOpacity(0.6),
            ),
          ),
        ),
        Positioned(
          left: 20,
          top: -20,
          child: Text(
            number,
            style: TextStyle(
              color: Color(0xffFFAE00).withOpacity(0.5),
              fontFamily: 'Poppins',
              fontSize: 125,
              fontWeight: FontWeight.w700
            )
          ),
        ),
        Positioned(
          top: top + 45,
          left: 40,
          child: Container(
            child: SvgPicture.asset(
              'assets/svgs/doublequotes.svg',
              color: Color(0xffFFAE00).withOpacity(0.5),
              height: 100,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(leftPadding, topPadding, 20.0, 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    departmentName,
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    height: 5,
                    width: 130,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(100)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Text(
                    departmentQuestion,
                    style: TextStyle(
                      color: Color(0xffFFAE00),
                      fontFamily: 'AvenirNext',
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    )
                  ), 
                  SizedBox(height: 10.0,),
                  Container(
                    width: 350,
                    height: MediaQuery.of(context).size.height * scrollHeight,
                    child: SingleChildScrollView(
                      child: Text(
                        description,
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: function,
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width:  MediaQuery.of(context).size.width * 0.45,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(20.0)
                      ),
                      child: Center(
                        child: Text(
                          'Explore this major',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0,)
                ],
              ),
            ],
          ),
        )
      ],
    );
  }
}
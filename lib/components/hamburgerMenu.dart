import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/about_us_screen/stackAboutUsScreen.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/stackScreenAdmissionInfo.dart';
import 'package:paragon_handbook/screens/faculty_screen/stackScreenFaculty.dart';
import 'package:paragon_handbook/screens/home_screen/stackScreenHomePage.dart';
import 'package:paragon_handbook/screens/school_event/stackScreenSchoolEvent.dart';
import 'package:paragon_handbook/screens/uni_service/stackScreenUniService.dart';

class HamburgerMenu extends StatefulWidget {
  @override
  _HamburgerMenuState createState() => _HamburgerMenuState();
}

class _HamburgerMenuState extends State<HamburgerMenu> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      color: Color(0xffFFFFFF),
      child: Column(
        children: [
          SizedBox(
            height: 120.0,
          ),
          HamburgerMenuItem(
            icon: Icons.home,
            name: 'Home',
            function: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => StackScreenHomePage()));
            },
          ),
          SizedBox(
            height: 20.0,
          ),
          HamburgerMenuItem(
            icon: Icons.info,
            name: 'Admission\nInformation',
            height: 70.0,
            function: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => StackScreenAdmissionInfo()));
            },
          ),
          SizedBox(
            height: 20.0,
          ),
          HamburgerMenuItem(
            icon: Icons.library_books,
            name: 'Faculties',
            function: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => StackScreenFaculty()));
            },
          ),
          SizedBox(
            height: 20.0,
          ),
          HamburgerMenuItem(
            icon: Icons.event_note,
            name: 'School\nEvents',
            height: 70.0,
            function: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => StackScreenSchoolEvent()));
            },
          ),
          SizedBox(
            height: 20.0,
          ),
          HamburgerMenuItem(
            icon: Icons.supervised_user_circle,
            name: 'University\nServices',
            height: 70.0,
            function: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => StackScreenUniService()));
            },
          ),
          SizedBox(height: 20.0,),
          HamburgerMenuItem(
            icon: Icons.account_circle,
            name: 'About the \ndevelopers',
            height: 70.0,
            function: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (context) => StackScreenAboutUs())
              );
            },
          ),
        ],
      ),
    );
  }
}

class HamburgerMenuItem extends StatelessWidget {
  final IconData icon;
  final String name;
  final double height;
  final double lineWidth;
  final Function function;

  const HamburgerMenuItem(
      {Key key,
      this.icon,
      this.name,
      this.function,
      this.height = 50.0,
      this.lineWidth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        width: MediaQuery.of(context).size.width * 0.5,
        height: height,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: Color(0xff192f59),
              size: 30.0,
            ),
            SizedBox(
              width: 10.0,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  name,
                  style: TextStyle(
                      color: Color(0xff192f59),
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}


import 'package:flutter/cupertino.dart';

class BulletPoints extends StatelessWidget {
  
  final String text;
  final double size;

  const BulletPoints({
    Key key, this.text, this.size = 15.0
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: 5),
          width: size,
          height: size,
          decoration: BoxDecoration(
            border: Border.all(color: Color(0xffFFAE00), width: 2),
            borderRadius: BorderRadius.circular(10.0)
          ),
        ),
        SizedBox(width: 10.0,),
        Container(
          width: MediaQuery.of(context).size.width * 0.8,
          child: Text(
            text,
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
        ),
      ],
    );
  }
}
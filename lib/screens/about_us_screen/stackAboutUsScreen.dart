import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/hamburgerMenu.dart';
import 'package:paragon_handbook/screens/about_us_screen/aboutUsScreen.dart';

class StackScreenAboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          HamburgerMenu(),
          AboutUsScreen(),
        ],
      ),
    );
  }
}

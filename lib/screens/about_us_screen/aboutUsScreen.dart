import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:paragon_handbook/components/person_card.dart';

class AboutUsScreen extends StatefulWidget {
  @override
  _AboutUsScreenState createState() => _AboutUsScreenState();
}

class _AboutUsScreenState extends State<AboutUsScreen> {
  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;

  bool isDrawerOpen = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        transform: Matrix4.translationValues(xOffset, yOffset, 0)
          ..scale(scaleFactor),
        duration: Duration(milliseconds: 200),
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            toolbarHeight: 60,
            backgroundColor: Color(0xffffffff),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                isDrawerOpen
                    ? IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Color(0xff192f59),
                        ),
                        onPressed: () {
                          setState(() {
                            xOffset = 0;
                            yOffset = 0;
                            scaleFactor = 1;
                            isDrawerOpen = false;
                          });
                        },
                      )
                    : Container(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              xOffset = 200;
                              yOffset = 100;
                              scaleFactor = 0.7;
                              isDrawerOpen = true;
                            });
                          },
                          child: SvgPicture.asset(
                            'assets/svgs/menu.svg',
                            height: 15.0,
                            color: Color(0xff192f59),
                          ),
                        ),
                      ),
                SizedBox(
                  width: 25.0,
                ),
                Image.asset(
                  'assets/images/logo_with_name_blue.png',
                  height: 40.0,
                )
              ],
            ),
          ),
          body: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding:
                        EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Our Team',
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 25.0,
                              color: Color(0xff192f59)),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.12,
                          height: 5.0,
                          decoration: BoxDecoration(
                              color: Color(0xffFFAE00),
                              borderRadius: BorderRadius.circular(10.0)),
                        ),
                      ],
                    ),
                  ),
                  PersonCard(
                    picture: 'assets/images/about_us_suling.jpg',
                    name: 'Prom Sokleng',
                    department: 'Computer Science\nSophomore',
                    color: Color(0xffEFEFEF),
                  ),
                  PersonCard(
                    picture: 'assets/images/about_us_phorith.jpg',
                    name: 'Bun Phorith',
                    department: 'Management Information\nSystem Sophomore',
                    color: Colors.white,
                  ),
                  PersonCard(
                    picture: 'assets/images/about_us_panharith.jpg',
                    name: 'Sun Panharith',
                    department: 'Computer Science\nSophomore',
                    color: Color(0xffEFEFEF),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding:
                        EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Our Work',
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 25.0,
                              color: Color(0xff192f59)),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.12,
                          height: 5.0,
                          decoration: BoxDecoration(
                              color: Color(0xffFFAE00),
                              borderRadius: BorderRadius.circular(10.0)),
                        ),
                      ],
                    ),
                  ),
                  Stack(
                    children: [
                      Image.asset('assets/images/about_us_grouppic.jpg'),
                      Container(
                        child: Opacity(
                          opacity: 0.3,
                          child: Container(
                            height: 275,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding:
                        EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Our team started learning Flutter through an intensive bootcamp hosted by a start-up called "Nest Innovation"',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'During a timespan of 1 month, we have learned to program many different mobile application using this open-source UI toolkit',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Our intensive learning has been developing comparing to the first duty day when we landed on Flutter Program. However, we are still lacking on a lot more view point toward the learning process and more onward to the programming langauge itself.',
                          style: TextStyle(fontSize: 16.0),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

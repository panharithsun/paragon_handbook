import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/hamburgerMenu.dart';
import 'homeScreen.dart';

class StackScreenHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          HamburgerMenu(),
          MyHomePage(),
        ],
      ),
    );
  }
}

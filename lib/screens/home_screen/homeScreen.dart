import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:paragon_handbook/screens/about_us_screen/stackAboutUsScreen.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/stackScreenAdmissionInfo.dart';
import 'package:paragon_handbook/screens/faculty_screen/stackScreenFaculty.dart';
import 'package:paragon_handbook/screens/school_event/stackScreenSchoolEvent.dart';
import 'package:paragon_handbook/screens/uni_service/stackScreenUniService.dart';
import 'package:paragon_handbook/screens/work_in_progress_screen/stackScreenWorkInProgress.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final controller = PageController(initialPage: 0);

  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;
  bool isDrawerOpen = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(xOffset, yOffset, 0)
        ..scale(scaleFactor),
      duration: Duration(milliseconds: 200),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          toolbarHeight: 60,
          backgroundColor: Color(0xffffffff),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              isDrawerOpen
                  ? IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Color(0xff192f59),
                      ),
                      onPressed: () {
                        setState(() {
                          xOffset = 0;
                          yOffset = 0;
                          scaleFactor = 1;
                          isDrawerOpen = false;
                        });
                      },
                    )
                  : GestureDetector(
                    onTap: () {
                      setState(() {
                        xOffset = 200;
                        yOffset = 100;
                        scaleFactor = 0.7;
                        isDrawerOpen = true;
                      });
                    },
                    child: Container(
                      child: SvgPicture.asset(
                        'assets/svgs/menu.svg',
                        height: 15.0,
                        color: Color(0xff192f59),
                      ),
                    ),
                  ),
              SizedBox(
                width: 25.0,
              ),
              Image.asset(
                'assets/images/logo_with_name_blue.png',
                height: 40.0,
              )
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: Image.asset('assets/images/students_homescreen.png',
                        fit: BoxFit.fill),
                  ),
                  Opacity(
                    opacity: 0.5,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                        color: Color(0xff192f59),
                      ),
                    ),
                  ),
                  Opacity(
                    opacity: 0.3,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(color: Colors.black),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 100.0, 20.0, 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 5,
                          width: 180,
                          color: Color(0xffFFAE00),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          child: Text(
                            'W E L C O M E  T O\n\nP A R A G O N\n\nI N T E R N A T I O N A L\n\nU N I V E R S I T Y',
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                color: Colors.white,
                                fontSize: 27,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          child: Text(
                            'Guide Line Handbook into the campus',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            'About our University',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w400),
                          ),
                        ),
                        Container(
                          height: 4.0,
                          width: MediaQuery.of(context).size.width * 0.28,
                          decoration: BoxDecoration(
                            color: Color(0xffFFAE00),
                            borderRadius: BorderRadius.circular(100)),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.info,
                          size: 40.0,
                          color: Color(0xffFFAE00),
                        ),
                        Icon(
                          Icons.library_books,
                          size: 40.0,
                          color: Color(0xffFFAE00),
                        ),
                        Icon(
                          Icons.event_note,
                          size: 40.0,
                          color: Color(0xffFFAE00),
                        ),
                        Icon(
                          Icons.supervised_user_circle,
                          size: 40.0,
                          color: Color(0xffFFAE00),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
              Stack(
                children: [
                  Container(
                    height: 300.0,
                    child: PageView(
                      scrollDirection: Axis.horizontal,
                      controller: controller,
                      children: [
                        PagesCard(
                          imageUrl: 'assets/images/paragoncampus.jpg',
                          title: 'T A K E  A\nC A M P U S  T O U R !',
                          description: 'Contact us to schedule a campus tour',
                          function: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StackScreenWorkInProgress()
                              )
                            );
                          },
                        ),
                        PagesCard(
                          imageUrl: 'assets/images/alumni.jpg',
                          title: 'N E W  A N D\nU P D A T E',
                          description: 'Class of 2019',
                          function: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StackScreenWorkInProgress()
                              )
                            );
                          },
                        ),
                        PagesCard(
                          imageUrl: 'assets/images/ApplyNowBG.jpg',
                          title: 'A P P L Y  F O R\nA D M I S S I O N',
                          description: 'Applications for Academic Year 2020-2021 are now open',
                          function: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StackScreenAdmissionInfo()
                              )
                            );
                          },
                        ),
                        PagesCard(
                          imageUrl: 'assets/images/ict_cs1.png',
                          title: 'O U R\nF A C U L T I E S',
                          description: 'Discover the different faculties in our university',
                          boxFit: BoxFit.fitWidth,
                          function: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StackScreenFaculty()
                              )
                            );
                          },
                        ),
                        PagesCard(
                          imageUrl: 'assets/images/soccer.jpg',
                          title: 'S C H O O L\nE V E N T S',
                          description: 'Different events hosted by our university',
                          function: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StackScreenSchoolEvent()
                              )
                            );
                          },
                        ),
                        PagesCard(
                          imageUrl: 'assets/images/uniservice_student_council.jpg',
                          title: 'U N I V E R S I T Y\nS E R V I C E',
                          description: 'Different services provided to our students',
                          function: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StackScreenUniService()
                              )
                            );
                          },
                        ),
                        PagesCard(
                          imageUrl: 'assets/images/about_us_grouppic.jpg',
                          title: 'A B O U T  T H E\nD E V E L O P E R S',
                          description: 'Find out informations about the developers behind this application',
                          // function: () {
                          //   Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //       builder: (context) => StackScreenAboutUs()
                          //     )
                          //   );
                          // },
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    right: 0.0,
                    child: Container(
                      height: 30.0,
                      width: 20.0,
                      child: Center(
                        child: SmoothPageIndicator(
                          controller: controller,
                          count: 7,
                          effect: WormEffect(
                            dotWidth: 10,
                            dotHeight: 10,
                            radius: 16,
                            dotColor: Colors.grey,
                            activeDotColor: Color(0xffFFAE00),
                          ),
                          onDotClicked: (int page) {
                            controller.animateToPage(
                              page,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.ease,
                            );
                          },
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class PagesCard extends StatelessWidget {

  final String imageUrl;
  final String title;
  final String description;
  final Function function;
  final BoxFit boxFit;

  const PagesCard({
    Key key, this.imageUrl, this.title, this.description, this.function, this.boxFit = BoxFit.fitHeight
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          child: Image.asset(
            imageUrl,
            height: 300,
            fit: boxFit,
          )
        ),
        Container(
          padding: EdgeInsets.only(right: 20.0, left: 20.0, top: 40.0, bottom: 20.0),
          width: MediaQuery.of(context).size.width,
          height: 300.0,
          color: Color(0xff192f59).withOpacity(0.6),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Text(
                      title,
                      style: TextStyle(
                        color: Colors.white, 
                        fontSize: MediaQuery.of(context).size.width * 0.08,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Text(
                      description,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontWeight: FontWeight.w400
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                  FlatButton(
                    onPressed: function,
                    child: Container(
                      width: 100,
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Color(0xffFFAE00),
                      ),
                      child: Center(
                        child: Text(
                          'Explore',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
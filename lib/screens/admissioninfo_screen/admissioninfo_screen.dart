import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/info_screens/admission_requirement.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/info_screens/scholarships.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/info_screens/tuition_fee.dart';

class AdmissionInfoScreen extends StatefulWidget {
  @override
  _AdmissionInfoScreenState createState() => _AdmissionInfoScreenState();
}

class _AdmissionInfoScreenState extends State<AdmissionInfoScreen> {

  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;

  bool isDrawerOpen = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(xOffset, yOffset, 0)
        ..scale(scaleFactor),
      duration: Duration(milliseconds: 200),
      child: Scaffold(
        // resizeToAvoidBottomPadding: false,
        // resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          toolbarHeight: 60,
          backgroundColor: Color(0xffffffff),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              isDrawerOpen  
              ?  IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Color(0xff192f59),),
                  onPressed: () {
                    setState(() {
                      xOffset = 0;
                      yOffset = 0;
                      scaleFactor = 1;
                      isDrawerOpen = false;
                    });
                  },
                )
              : Container(
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      xOffset = 200;
                      yOffset = 100;
                      scaleFactor = 0.7;
                      isDrawerOpen = true;
                    });
                  },
                  child: SvgPicture.asset(
                    'assets/svgs/menu.svg',
                    height: 15.0,
                    color: Color(0xff192f59),
                  ),
                ),
              ),
              SizedBox(
                width: 25.0,
              ),
              Image.asset(
                'assets/images/logo_with_name_blue.png',
                height: 40.0,
              )
            ],
          ),
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Positioned(
                    child: Container(
                      height: 500,
                      width: 500,
                    ),
                  ),
                  Positioned(
                    left: 20,
                    child: Container(
                      child: TyperAnimatedTextKit(
                        text: ["A D M I S S I O N  \nI N F O R M A T I O N"],
                        textStyle: TextStyle(
                          fontFamily: 'AvenirNext',
                          fontSize: 30.0,
                          fontWeight: FontWeight.w400,
                        ),)
                      ),
                  ),
                  Positioned(
                    top: 120,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 125,
                      child: Image.asset(
                        'assets/images/ApplyNowBG.jpg',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 120,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AdmissionRequirement())
                        );
                      },
                      child: Opacity(
                        opacity: 0.5,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          color: Color(0xff192f59),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 50,
                    top: 170,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AdmissionRequirement())
                        );
                      },
                      child: Text('Admission Requirement',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w600,
                          )),
                    ),
                  ),
                  Positioned(
                    top: 245,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 125,
                      child: Image.asset(
                        'assets/images/tuition-fee.jpg',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 245,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => TuitionFee())
                        );
                      },
                      child: Opacity(
                        opacity: 0.5,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          color: Color(0xff192f59),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 50,
                    top: 292,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => TuitionFee())
                        );
                      },
                      child: Text('Tutition Fee',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w600,
                        )
                      ),
                    ),
                  ),
                  Positioned(
                    top: 370,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 125,
                      child: Image.asset(
                        'assets/images/scholarship.jpg',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 370,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Scholarship())
                        );
                      },
                      child: Opacity(
                        opacity: 0.5,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          color: Color(0xff192f59),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 50,
                    top: 420,
                    child: GestureDetector(
                       onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Scholarship())
                        );
                      },
                      child: Text('Scholarships',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w600,
                        )
                      ),
                    ),
                  ),
                  Positioned(
                    left: 16.5,
                    top: 175,
                    child: Container(
                      height: 20,
                      width: 20,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 16.5,
                    top: 300,
                    child: Container(
                      height: 20,
                      width: 20,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 16.5,
                    top: 425,
                    child: Container(
                      height: 20,
                      width: 20,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 22.5,
                    top: 80,
                    child: Container(
                      height: 550,
                      width: 7.5,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                ],
                overflow: Overflow.visible,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/hamburgerMenu.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/admissioninfo_screen.dart';

class StackScreenAdmissionInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        HamburgerMenu(),
        AdmissionInfoScreen(),
      ],
      ),
    );
  }
}
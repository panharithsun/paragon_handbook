import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/application_process.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/what_you_need_to_know.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/when_you_need_to_apply.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/where_to_submit.dart';

class AdmissionRequirement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ApplicationProcess(),
            WhatYouNeedToKnow(),
            WhenYouNeedToApply(),
            WhereToSubmit()
          ],
        ),
      ),
    );
  }
}




import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/FAQ.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/national_exam_scholarships.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/national_olypiad_scholarships.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/paragonu_scholarships.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/partner_scholarship.dart';

class Scholarship extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ParagonUScholarships(),
          PartnershipScholarships(),
          NationalOlypiadsScholarships(),
          NationalExamGradeScholarships(),
          FrequentlyAskedQuestions(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Contact For Scholarship',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        )),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.47,
                      height: 5.0,
                      decoration: BoxDecoration(
                          color: Color(0xffFFAE00),
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
                width: MediaQuery.of(context).size.width,
                color: Color(0xffEFEFEF),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'We are open from Monday to Friday, from 8 am-5 pm.',
                      style: TextStyle(fontSize: 16.0),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      'Phones: ',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '023 996 111',
                      style: TextStyle(fontSize: 16.0),
                    ),
                    Text(
                      '017 996 111',
                      style: TextStyle(fontSize: 16.0),
                    ),
                    Text(
                      '015 996 111',
                      style: TextStyle(fontSize: 16.0),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      'Email: ',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'admission@paragoniu.edu.kh',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      )),
    );
  }
}

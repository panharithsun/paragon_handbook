import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/cost_of_living.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/postgraduate_tuitionfee.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/components/undergraduate_tuitionfee.dart';

class TuitionFee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UndergraduateTuitionFee(),
            PostgraduateTuitionFee(),
            CostOfLiving()
          ],
        ),
      ),
    );
  }
}


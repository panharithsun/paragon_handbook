import 'package:flutter/cupertino.dart';
import 'package:paragon_handbook/components/bullet_point.dart';

class WhatYouNeedToKnow extends StatelessWidget {
  const WhatYouNeedToKnow({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
      color: Color(0xffEFEFEF),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'What you need to know',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 25.0,
              color: Color(0xff192f59)
            ),
          ),
          SizedBox(height: 10.0,),
          Container(
            width: MediaQuery.of(context).size.width * 0.49,
            height: 5.0,
            decoration: BoxDecoration(
              color: Color(0xffFFAE00),
              borderRadius: BorderRadius.circular(10.0)
            ),
          ),
          SizedBox(height: 20.0,),
          Text(
            'To be admitted to Paragon International University, all applicants must meet the minimum requirements as follows:',
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          SizedBox(height: 20),
          BulletPoints(
            text: 'High school diploma or a letter from your school certifying that you graduated or will graduate this year.',
          ),
          SizedBox(height: 15.0,),
          BulletPoints(
            text: 'A passing grade from the Grade 12 Exam (BAC II) for Cambodian applicants',
          ),
          SizedBox(height: 15.0,),
          BulletPoints(
            text: 'Passport or Cambodian National ID card',
          ),
          SizedBox(height: 15.0,),
          BulletPoints(
            text: '4 photos (size 4*6)',
          ),
          SizedBox(height: 15.0,),
          BulletPoints(
            text: 'Completed Application Form (The form can be obtained at the Front Desk on campus for \$10)',
          ),
        ],
      ),
    );
  }
}



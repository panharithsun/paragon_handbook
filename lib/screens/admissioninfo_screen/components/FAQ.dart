import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FrequentlyAskedQuestions extends StatelessWidget {
  const FrequentlyAskedQuestions({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Frequently Asked Questions',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                )
              ),
              SizedBox(height: 10.0,),
              Container(
                width: MediaQuery.of(context).size.width * 0.52,
                height: 5.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
              ),
            ],
          ),
        ),
        Card(
          elevation: 0,
          child: ExpansionTile(
            title: Text(
              'What is the scholarship exam like?',
              style: TextStyle(
                color: Color(0xff192f59),
                fontWeight: FontWeight.w600,
                fontSize: 18.0
              ),
            ),
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 17.0, vertical: 10.0),
                child: Text(
                  'The Paragon International University Scholarship Exam is a 2-hour long, paper-based, multiple-choice test that measures students’ general ability to succeed in one or more study programs at Paragon International University. There are three exam types: General Reasoning, International Relations, and Physics. \n\n* Applicants interested in any field other than International Relations must take the General Reasoning Exam, which consists of Mathematical Reasoning, Logical Reasoning, and Verbal Reasoning questions. This is a two-hour long exam. \n\n* Applicants interested in Civil Engineering additionally take the Physics Exam. This is a one-hour exam. \n\n* Applicants interested in International Relations (regardless of any other interests) must take the International Relations Exam. This is a two-hour long exam. It is only for IR applicants and consists primarily of questions testing general knowledge of current events and history. It also contains a short section on logical reasoning.',
                  style: TextStyle(
                    fontSize: 16.0
                  ), 
                ),
              ),
            ],
          ),
        ),
        Card(
          elevation: 0,
          child: ExpansionTile(
            title: Text(
              'Does the scholarship include the registration fee?',
              style: TextStyle(
                color: Color(0xff192f59),
                fontWeight: FontWeight.w600,
                fontSize: 18.0
              ),
            ),
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 17.0, vertical: 10.0),
                child: Text(
                  'The scholarships offered at Paragon International University are discounts that cover only tuition fees, not the registration fee. For example, a student who has earned a 100 % scholarship has to pay only a registration fee every year.',
                  style: TextStyle(
                    fontSize: 16.0
                  ), 
                ),
              ),
            ],
          ),
        ),
        Card(
          elevation: 0,
          child: ExpansionTile(
            title: Text(
              'If I miss the Scholarship Examination day, can I take the exam on another date?',
              style: TextStyle(
                color: Color(0xff192f59),
                fontWeight: FontWeight.w600,
                fontSize: 18.0
              ),
            ),
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 17.0, vertical: 10.0),
                child: Text(
                  'No, Paragon International University Scholarship Examinations are administered only once every academic year.',
                  style: TextStyle(
                    fontSize: 16.0
                  ), 
                ),
              ),
            ],
          ),
        ),
        Card(
          elevation: 0,
          child: ExpansionTile(
            title: Text(
              'I am an international student. Can I apply for a Paragon International University Scholarship Exam?',
              style: TextStyle(
                color: Color(0xff192f59),
                fontWeight: FontWeight.w600,
                fontSize: 18.0
              ),
            ),
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 17.0, vertical: 10.0),
                child: Text(
                  'Yes, International applicants residing in Cambodia can apply for scholarships via the Scholarship Exam.',
                  style: TextStyle(
                    fontSize: 16.0
                  ), 
                ),
              ),
            ],
          ),
        ),
        Card(
          elevation: 0,
          child: ExpansionTile(
            title: Text(
              'Can I still retain my scholarship if I have to study in the English Preparatory Program?',
              style: TextStyle(
                color: Color(0xff192f59),
                fontWeight: FontWeight.w600,
                fontSize: 18.0
              ),
            ),
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 17.0, vertical: 10.0),
                child: Text(
                  'Yes, a tuition fee scholarship is valid for 2 semesters spent at the English preparatory school and 10 semesters spent at the undergraduate program. Notice that the tuition fee scholarship does not include the registration fee.',
                  style: TextStyle(
                    fontSize: 16.0
                  ), 
                ),
              ),
            ],
          ),
        ),
        Divider(color: Color(0xffEFEFEF), thickness: 2,)
      ],
    );
  }
}
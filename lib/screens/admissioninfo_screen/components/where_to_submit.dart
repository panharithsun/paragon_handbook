import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WhereToSubmit extends StatelessWidget {
  const WhereToSubmit({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
      color: Color(0xffEFEFEF),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Where to submit neccessary documents',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 25.0,
              color: Color(0xff192f59)
            ),
          ),
          SizedBox(height: 10.0,),
          Container(
            width: MediaQuery.of(context).size.width * 0.49,
            height: 5.0,
            decoration: BoxDecoration(
              color: Color(0xffFFAE00),
              borderRadius: BorderRadius.circular(10.0)
            ),
          ),
          SizedBox(height: 20.0,),
          Text(
            'Documents should be submitted to the Office of the Registrar at Paragon International University.',
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          SizedBox(height: 20.0,),
          RichText(
            text: TextSpan(
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 16.0,
                color: Colors.black,
              ),
              children: <TextSpan> [
                TextSpan(
                  text: 'You can fill', 
                ),
                TextSpan(
                  text: ' online application form ', style: TextStyle(fontWeight: FontWeight.bold)
                ),
                TextSpan(
                  text: 'and bring original documents to the Office of the Registrar. ',
                )
              ]
            )
          ),
          SizedBox(height: 20.0),
          Text(
            'No. 8',
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          Text(
            'St. 315, Boeng Kak 1, Tuol Kork',
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          Text(
            'Phnom Penh, Cambodia',
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          SizedBox(height: 20.0),
          RichText(
            text: TextSpan(
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 16.0,
                color: Colors.black,
              ),
              children: <TextSpan> [
                TextSpan(
                  text: 'In case you are unable to submit documents in person, please contact us at', 
                ),
                TextSpan(
                  text: ' +855-23-996-111, +855-17-996-111, or +855-15-996-111. ', style: TextStyle(color: Color(0xff192f59))
                ),
              ]
            )
          ),
          SizedBox(height: 10.0,),
          RichText(
            text: TextSpan(
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 16.0,
                color: Colors.black,
              ),
              children: <TextSpan> [
                TextSpan(
                  text: 'Email us:', 
                ),
                TextSpan(
                  text: ' admission@paragoniu.edu.kh ', style: TextStyle(color: Color(0xff192f59))
                ),
              ]
            )
          ),
        ],
      ),
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';

class UndergraduateTuitionFee extends StatelessWidget {
  const UndergraduateTuitionFee({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Undergraduate Degree',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                )
              ),
              SizedBox(height: 10.0,),
              Container(
                width: MediaQuery.of(context).size.width * 0.47,
                height: 5.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffEFEFEF),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Faculty of Engineering',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                )
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of Architecture' ,
                size: 12.0
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of Civil Engineering' ,
                size: 12.0
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of Industrial Engineering' ,
                size: 12.0
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                width: MediaQuery.of(context).size.width * 0.55,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
                child: Center(
                  child: Text(
                    '2,000 USD Per Semester',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Faculty of Information and Computer Technologies',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                )
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of Computer Science' ,
                size: 12.0
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of Management of Information System' ,
                size: 12.0
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                width: MediaQuery.of(context).size.width * 0.55,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
                child: Center(
                  child: Text(
                    '1,500 USD Per Semester',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffEFEFEF),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Faculty of Economics and Administrative Sciences',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                )
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of Banking and Finance' ,
                size: 12.0
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of Business Administration' ,
                size: 12.0
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of International Trade and Logistics',
                size: 12.0
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Department of Political Science and International Relations',
                size: 12.0
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                width: MediaQuery.of(context).size.width * 0.55,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
                child: Center(
                  child: Text(
                    '1,250 USD Per Semester',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'English Preparatory School',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                )
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                width: MediaQuery.of(context).size.width * 0.55,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
                child: Center(
                  child: Text(
                    '1,000 USD Per Semester',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Divider(color: Color(0xffEFEFEF), thickness: 2),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'NOTE:',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(height: 20.0,),
              Text(
                'In addition to the tuition fee, all students are required to pay a \$500 registration fee at the beginning of each academic year.',
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
              SizedBox(height: 20.0,),
              RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontFamily: 'Poppins'
                  ),
                  children: [
                    TextSpan(
                      text: 'To help students finance their studies, Paragon International University offers a wide range of ',
                    ),
                    TextSpan(
                      text: 'scholarships ',
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      )
                    ),
                    TextSpan(
                      text: '.',
                    )
                  ]
                ),
              )
            ],
          ),
        ),
        Divider(color: Color(0xffEFEFEF), thickness: 2),
      ],
    );
  }
}
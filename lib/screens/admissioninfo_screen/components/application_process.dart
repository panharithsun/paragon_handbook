import 'package:flutter/cupertino.dart';

class ApplicationProcess extends StatelessWidget {
  const ApplicationProcess({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'The Application Process',
            style: TextStyle(
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w700,
                fontSize: 25.0,
                color: Color(0xff192f59)),
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.49,
            height: 5.0,
            decoration: BoxDecoration(
                color: Color(0xffFFAE00),
                borderRadius: BorderRadius.circular(10.0)),
          ),
          SizedBox(
            height: 20.0,
          ),
          ApplicationProcessStep(
            index: '1',
            title: 'Fill out Enrollment Form',
            description:
                'Fill out your personal information in our Enrollment Form which could be collected and filled at the University Front Desk.',
          ),
          ApplicationProcessStep(
            index: '2',
            title: 'Pay Registration Fee',
            description:
                'Registration fee is an annual fee which students have to pay at the beginning of each academic year.',
          ),
          ApplicationProcessStep(
            index: '3',
            title: 'Take English Proficiency Test',
            description:
                'Our team will inform you of a booked test date after you have submitted your Enrollment Form and paid Registration Fee',
          ),
          ApplicationProcessStep(
              index: '4',
              title: 'Pay Tuition Fee',
              description:
                  'The cost of tuition fee varies depending on your selected department.'),
          ApplicationProcessStep(
            index: '5',
            title:
                'Start Foundation Year Studies or English Preparatory School',
            description:
                'Fill out your personal information in our Enrollment Form which could be collected and filled at the University Front Desk.',
          ),
        ],
      ),
    );
  }
}

class ApplicationProcessStep extends StatelessWidget {
  final String index;
  final String title;
  final String description;

  const ApplicationProcessStep({
    Key key,
    this.index,
    this.title,
    this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            index,
            style: TextStyle(
              fontSize: 40.0,
              fontWeight: FontWeight.w700,
              color: Color(0xffFFAE00),
            ),
          ),
          SizedBox(
            width: 15.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 10.0,
              ),
              Container(
                width: 275,
                child: Text(
                  title,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff192f59),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                width: 280,
                height: 80.0,
                child: Text(description,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                    )),
              ),
            ],
          )
        ],
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';

class PartnershipScholarships extends StatelessWidget {
  const PartnershipScholarships({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Partnership Scholarships',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                )
              ),
              SizedBox(height: 10.0,),
              Container(
                width: MediaQuery.of(context).size.width * 0.47,
                height: 5.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffEFEFEF),
          child: Text(
            'Students who have graduated or will graduate from one of our partner high schools are eligible to apply for our Partnership Scholarship. Number of scholarships, terms and conditions, and Scholarships form and registration could be found at your school’s administrator. Partners of Paragon.U are Paragon International School, NTC Group, SIS International School, and The Westline School.',
            style: TextStyle(
              fontSize: 16.0
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Eligibility:',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                ),
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'A registered student and has graduated from one of the high school partners of ParagonU',
                size: 12.0,
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Be selected and nominated by one of the high school partners of ParagonU',
                size: 12.0,
              ),
              SizedBox(height: 20.0,),
              Text(
                'Procedures:',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                ),
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Apply for ParagonU Partnership Scholarship at your high school',
                size: 12.0,
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Start the Enrollment Procedure at ParagonU after your name has been selected and nominated to ParagonU',
                size: 12.0,
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffFFAE00),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Number of Scholarships:',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: Colors.white
                ),
              ),
              SizedBox(height: 20.0,),
              Text(
                'Scholarship provision is upon agreement between ParagonU and high school partners (including Paragon International School, NTC Group, SIS International School, and The Westline Education)',
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.white
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
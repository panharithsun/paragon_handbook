import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CostOfLiving extends StatelessWidget {
  const CostOfLiving({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Cost of Living in Phnom Penh',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                )
              ),
              SizedBox(height: 10.0,),
              Container(
                width: MediaQuery.of(context).size.width * 0.47,
                height: 5.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
              ),
              SizedBox(height: 20.0,),
              RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 16.0,
                    fontFamily: 'Poppins',
                    color: Colors.black
                  ),
                  children: [
                    TextSpan(
                      text: 'Visa: ',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      )
                    ),
                    TextSpan(
                      text: 'Letter of Acceptance will be issued as a supporting document for your visa application.',
                    )
                  ]
                ),
              ),
              SizedBox(height: 20.0,),
              Text(
                'In addition to tuition fees, students will require money to cover their living costs including accommodation, food, books, entertainment, clothing, phone bills, local travel, and laundry. These expenses will vary depending on your lifestyle and spending habits. In keeping with ParagonIU rules, we recommend that students budget \$800 per month to cover their living costs.',
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black
                ),
              ),
              SizedBox(height: 30.0,),
              Table(
                border: TableBorder.all(width: 1, color: Colors.white),
                children: [
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffFFAE00),
                        child: Center(
                          child: Text(
                            'Expense Type',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffFFAE00),
                        child: Center(
                          child: Text(
                            'Amount per month',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffEFEFEF),
                        child: Center(
                          child: Text(
                            'Housing',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffEFEFEF),
                        child: Center(
                          child: Text(
                            '\$200 – \$300',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffF9F9F9),
                        child: Center(
                          child: Text(
                            'Local Transportation',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffF9F9F9),
                        child: Center(
                          child: Text(
                            '\$50 – \$100',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffEFEFEF),
                        child: Center(
                          child: Text(
                            'Meals',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffEFEFEF),
                        child: Center(
                          child: Text(
                            '\$200 – \$250',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffF9F9F9),
                        child: Center(
                          child: Text(
                            'Books/school supplies',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffF9F9F9),
                        child: Center(
                          child: Text(
                            '\$200 per semester',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffEFEFEF),
                        child: Center(
                          child: Text(
                            'Telecommunication',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffEFEFEF),
                        child: Center(
                          child: Text(
                            '\$10',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffF9F9F9),
                        child: Center(
                          child: Text(
                            'Personal expenses',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffF9F9F9),
                        child: Center(
                          child: Text(
                            '\$100',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff545454),
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';

class ParagonUScholarships extends StatelessWidget {
  const ParagonUScholarships({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'ParagonU Scholarships',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                )
              ),
              SizedBox(height: 10.0,),
              Container(
                width: MediaQuery.of(context).size.width * 0.47,
                height: 5.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffEFEFEF),
          child: Text(
            'Students who passed the high school national exam from any academic year or those who will take it this year can apply for the Paragon International University Scholarship Examination. Due to COVID 19, the Scholarship Examination for 2020 was conducted online on September 12.',
            style: TextStyle(
              fontSize: 16.0
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Eligibility:',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                ),
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Students who passed the high school examination or will take it this year',
                size: 12.0,
              ),
              SizedBox(height: 20.0,),
              Text(
                'Procedures:',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                ),
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Fill out a Scholarship Exam Application Form before the deadline.',
                size: 12.0,
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Pay the exam fee.',
                size: 12.0,
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Take the exam on specified date.',
                size: 12.0,
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffFFAE00),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Number of Scholarships:',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: Colors.white
                ),
              ),
              SizedBox(height: 20.0,),
              Table(
                border: TableBorder.all(width: 1, color: Color(0xffFFAE00)),
                children: [
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffEFEFEF),
                        child: Center(
                          child: Text(
                            'Tuition Fee Coverage',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Color(0xff192f59),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Color(0xffEFEFEF),
                        child: Center(
                          child: Text(
                            'Number of Awards',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Color(0xff192f59),
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Colors.white,
                        child: Center(
                          child: Text(
                            '100%',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Colors.white,
                        child: Center(
                          child: Text(
                            '30',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Colors.white,
                        child: Center(
                          child: Text(
                            '75%',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Colors.white,
                        child: Center(
                          child: Text(
                            '50',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Colors.white,
                        child: Center(
                          child: Text(
                            '50%',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Colors.white,
                        child: Center(
                          child: Text(
                            '100',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Colors.white,
                        child: Center(
                          child: Text(
                            '25%',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 50.0,
                        color: Colors.white,
                        child: Center(
                          child: Text(
                            '150',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ]
                  )
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
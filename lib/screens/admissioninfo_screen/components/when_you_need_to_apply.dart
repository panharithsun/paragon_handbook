import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WhenYouNeedToApply extends StatelessWidget {
  const WhenYouNeedToApply({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'When you need to apply',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 25.0,
              color: Color(0xff192f59)
            ),
          ),
          SizedBox(height: 10.0,),
          Container(
            width: MediaQuery.of(context).size.width * 0.49,
            height: 5.0,
            decoration: BoxDecoration(
              color: Color(0xffFFAE00),
              borderRadius: BorderRadius.circular(10.0)
            ),
          ),
          SizedBox(height: 10.0,),
          Table(
            border: TableBorder.all(width: 1, color: Colors.white),
            children: [
              TableRow(
                children: [
                  Container(
                    height: 70.0,
                    color: Color(0xffFFAE00),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffFFAE00),
                    child: Text(
                      'Open for application',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffFFAE00),
                    child: Text(
                      'Application Deadline',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  
                ]
              ),
              TableRow(
                children: [
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffEFEFEF),
                    child: Text(
                      'Scholarship Exam',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff545454),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffEFEFEF),
                    child: Center(
                      child: Text(
                        'August 1',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff545454),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffEFEFEF),
                    child: Center(
                      child: Text(
                        'September 1',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff545454),
                        ),
                      ),
                    ),
                  ),
                ]
              ),
              TableRow(
                children: [
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffF9F9F9),
                    child: Center(
                      child: Text(
                        'Semester 1',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff545454),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffF9F9F9),
                    child: Center(
                      child: Text(
                        'August 1',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff545454),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffF9F9F9),
                    child: Text(
                      'Early November',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff545454),
                      ),
                    ),
                  ),
                ]
              ),
              TableRow(
                children: [
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffEFEFEF),
                    child: Center(
                      child: Text(
                        'Semester 2',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff545454),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffEFEFEF),
                    child: Text(
                      'Early December',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff545454),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10.0),
                    height: 70.0,
                    color: Color(0xffEFEFEF),
                    child: Text(
                      'Early Febuary',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff545454),
                      ),
                    ),
                  ),
                ]
              ),
            ],
          )
        ],
      ),
    );
  }
}


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';

class NationalOlypiadsScholarships extends StatelessWidget {
  const NationalOlypiadsScholarships({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'National Olympiads Scholarships',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                )
              ),
              SizedBox(height: 10.0,),
              Container(
                width: MediaQuery.of(context).size.width * 0.47,
                height: 5.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffEFEFEF),
          child: Text(
            'Students who passed the high school national exam and are awardees of the National Academic Olympiads ranked from 1 – 10 for Math, Physics, and Khmer Literature categories in the most recent academic year are eligible to receive a full tuition fee scholarship from Paragon International University.',
            style: TextStyle(
              fontSize: 16.0
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Eligibility:',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                ),
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'National Academic Olympiads ranked from 1 – 10 for subjects including Math, Physics, and Khmer Literature',
                size: 12.0,
              ),
              SizedBox(height: 20.0,),
              Text(
                'Procedures:',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 20.0,
                  color: Color(0xff192f59)
                ),
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Provide Proof of the National Academic Olympiads Award to our Staff',
                size: 12.0,
              ),
              SizedBox(height: 10.0,),
              BulletPoints(
                text: 'Start the enrollment processes',
                size: 12.0,
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffFFAE00),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Number of Scholarships:',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: Colors.white
                ),
              ),
              SizedBox(height: 20.0,),
              Text(
                'To be announced',
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.white
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/info_screens/admission_requirement.dart';
import 'package:paragon_handbook/components/course_card.dart';

class CSDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'What career pathways are open to graduates of the CS program?',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 25.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.49,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Text(
                    'The CS graduates qualify for responsible positions in the IT industry, public and private sectors. Prospective employers include, but are not limited to, database administrators, IT security management, computer gaming sector, and various government agencies. Many of the CS graduates gain employment in technological startups, and some of them choose to focus on entrepreneurship. Regardless of the specialization, the Cambodian IT industry is booming with opportunities for qualified professionals, as this is one of the fastest-developing sectors in the Kingdom.',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Some of the examples of possible employment include:',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  BulletPoints(
                    text: 'Management Information Systems (IT Consultant, Database Administrator, Database Analyst)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Networks (Network Administrator, Network Engineer, Network Operations Project Manager)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Software Development (Fullstack Web Developer, Mobile Developer, Software Engineer, Software Architect, Programming Analyst)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Data Mining (Data Analyst)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Systems (Systems Administrator, Systems Engineer)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Telecommunications (Telecommunications Network Engineer)',
                  ),
                  SizedBox(height: 20.0,),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context) => AdmissionRequirement())
                      );
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width:  MediaQuery.of(context).size.width * 0.35,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(20.0)
                      ),
                      child: Center(
                        child: Text(
                          'Apply Now',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sample Curriculum',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 26.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER I',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 101',
                    courseName: 'Academic English I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ECON 100',
                    courseName: 'Introductory Economics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 101',
                    courseName: 'Khmer Studies I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 130',
                    courseName: 'Pre-Calculus',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 125',
                    courseName: 'Principles of Programming I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 120',
                    courseName: 'PCs, Internet, Networked Society',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER II',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 102',
                    courseName: 'Academic English II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ECON 201',
                    courseName: 'Microeconomics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 102',
                    courseName: 'Khmer Studies II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 131',
                    courseName: 'Calculus I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 126',
                    courseName: 'Principles of Programming II',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER III',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CS 201',
                    courseName: 'Data Structures and Algorithms',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 121',
                    courseName: 'Computer Architecture',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 260',
                    courseName: 'Web Design & Development',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 230',
                    courseName: 'Computer Networks',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 241',
                    courseName: 'Linear Algebra',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER IV',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CS 224',
                    courseName: 'Object Oriented Programming',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 211',
                    courseName: 'Software Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 250',
                    courseName: 'Operating Systems',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 262',
                    courseName: 'Advanced Concepts in Web Dev',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 250',
                    courseName: 'Probability and Statistics',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER V',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CS 311',
                    courseName: 'Discrete Mathematics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 221',
                    courseName: 'Media Computation',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 312',
                    courseName: 'Strength of Materials II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 321',
                    courseName: 'Database Management',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'DE',
                    courseName: 'Department Elective',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VI',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CS 340',
                    courseName: 'Self Study Programming System',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 342',
                    courseName: 'Artificial Intelligence',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 360',
                    courseName: 'Develop Mobile App for Android',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 382',
                    courseName: 'Search Engines and Info Retrieval',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 480',
                    courseName: 'Professionalism in Computing',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 332',
                    courseName: 'Switching, Routing and Wireless',
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CS 401',
                    courseName: 'Final Year Project I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IE 311',
                    courseName: 'System Analysis',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 468',
                    courseName: 'IT Project Management',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'DE',
                    courseName: 'Department Elective',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'DE',
                    courseName: 'Department Elective',
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VIII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CS 402',
                    courseName: 'Final Year Project II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'DE',
                    courseName: 'Department Elective',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'DE',
                    courseName: 'Department Elective',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'DE',
                    courseName: 'Department Elective',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
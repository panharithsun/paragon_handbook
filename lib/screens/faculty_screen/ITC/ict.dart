import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/faculty_screen/ITC/cs_detail_screen.dart';
import 'package:paragon_handbook/screens/faculty_screen/ITC/mis_detail_screen.dart';
import 'package:paragon_handbook/components/department_info.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class ICTpage extends StatelessWidget {
  final controller = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 50,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: Center(
        child: Stack(
          children: [
            PageView(
              controller: controller,
              children: <Widget>[
                DepartmentInformation(
                  number: '01',
                  imgUrl: 'assets/images/ict_cs1.png',
                  departmentName:
                      'D E P A R T M E N T  O F \nC O M P U T E R  S C I E N C E',
                  departmentQuestion:
                      'What is the focus of the Computer Science (CS) program?',
                  description:
                      'Recently the Royal Government of Cambodia has announced a set of policies aiming towards Industry 4.0. This is a global trend of using technology for automation and sustainability. Since this announcement, Cambodian businesses have been very interested in adopting technological solutions to optimize their processes and create new business models. Indeed, the number of tech startup companies in the Kingdom has been increasing rapidly in the past decade. However, many companies seeking to hire specialists in the field of computer science notice a shortage in the Cambodian labor market.',
                  function: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => CSDetailScreen()));
                  },
                ),
                DepartmentInformation(
                  topPadding: 28.0,
                  leftPadding: 25.0,
                  number: '02',
                  imgUrl: 'assets/images/ict_mis.png',
                  departmentName:
                      'D E P A R T M E N T  O F \nM A N A G E M E N T  O F  \nI N F O R M A T I O N  S Y S T E M',
                  departmentQuestion:
                      'What is the focus of the Management \nInformation System (MIS) program?',
                  description:
                      'Since the beginning of the Information Revolution, companies have been actively trying to use technology in their workflow. Nowadays they widely use the Internet for a lot of different purposes – from simple communication through email or videoconferencing, to managing customer information or financial modeling through complex software. Indeed, the information systems help businesses build better marketing strategies, improve customer experiences, and quickly obtain feedback. All these things increase the overall competitiveness of the companies and make information technology a highly important field. \n\nAn information system collects, processes, stores, analyzes and delivers information for a specific purpose. In short, it gets the right information to the right people, at the right time, in the right amount, and in the right format. Currently, MIS specialists are in very high demand, as they help companies find solutions that combine elements of management, organization, and technology.',
                  top: 150.0,
                  function: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MISDetailScreen()));
                  },
                ),
              ],
            ),
            Positioned(
              bottom: 5.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                height: 30.0,
                width: 20.0,
                child: Center(
                  child: SmoothPageIndicator(
                    controller: controller,
                    count: 2,
                    effect: WormEffect(
                      dotWidth: 10,
                      dotHeight: 10,
                      radius: 16,
                      dotColor: Colors.grey,
                      activeDotColor: Color(0xffFFAE00),
                    ),
                    onDotClicked: (int page) {
                      controller.animateToPage(
                        page,
                        duration: Duration(milliseconds: 400),
                        curve: Curves.ease,
                      );
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/info_screens/admission_requirement.dart';
import 'package:paragon_handbook/components/course_card.dart';

class MISDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'What career pathways are open to graduates of the MIS program?',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 25.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.49,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Text(
                    'The MIS program gives a strong competitive advantage to our graduates in the Cambodian labor market. Namely, young professionals get hired at IT departments of well-known companies, marketing agencies, web development studios or choose to pursue their entrepreneurship goals by founding IT startups.',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Our MIS graduates  often find themselves in the following fields:',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  BulletPoints(
                    text: 'Data Management (Data Analyst, Data Scientist, Database Administrator, IT Manager, Business Analyst)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Digital Marketing (Marketing Strategist, Search Engine Optimization Specialist)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'System Management (Computer Systems Administrator)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Information Security (Information Security Analyst)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Development (App Developer, Software Engineer, Web Developer)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Entrepreneurship (IT Entrepreneur, Startup Founder)',
                  ),
                  SizedBox(height: 20.0,),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context) => AdmissionRequirement())
                      );
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width:  MediaQuery.of(context).size.width * 0.35,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(20.0)
                      ),
                      child: Center(
                        child: Text(
                          'Apply Now',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sample Curriculum',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 26.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER I',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 101',
                    courseName: 'Academic English I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ECON 100',
                    courseName: 'Introductory Economics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 101',
                    courseName: 'Khmer Studies I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 111',
                    courseName: 'Essentials of Mathematics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 120',
                    courseName: 'PCs, Internet, Networked Society',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER II',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 102',
                    courseName: 'Academic English II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ECON 201',
                    courseName: 'Microeconomics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 102',
                    courseName: 'Khmer Studies II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 112',
                    courseName: 'IT Apps for Business Purposes',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 125',
                    courseName: 'Introduction to Java',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER III',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ACC 201',
                    courseName: 'Principles of Accounting I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'BUS 201',
                    courseName: 'Management I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 260',
                    courseName: 'Web Design & Development',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 201',
                    courseName: 'Management Info Systems',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'STAT 201',
                    courseName: 'Business Statistics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 422',
                    courseName: 'e-Business',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER IV',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ACC 202',
                    courseName: 'Principles of Accounting II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'BUS 202',
                    courseName: 'Management II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'BUS 212',
                    courseName: 'Business Analytics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'BUS 230',
                    courseName: 'Business Communications',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 262',
                    courseName: 'Advanced Concepts in Web Dev',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER V',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'BAF 300',
                    courseName: 'Business Finance',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 321',
                    courseName: 'Database Management',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 300',
                    courseName: 'MIS Junior Project 1',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 321',
                    courseName: 'Project Management',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 230',
                    courseName: 'Computer Networks',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VI',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'BUS 410',
                    courseName: 'Entrepreneurship',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 360',
                    courseName: 'Develop Mobile Apps for Android',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 301',
                    courseName: 'MIS Junior Project 2',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 211',
                    courseName: 'Software Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 430',
                    courseName: 'Professional Business Writing',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'BUS 420',
                    courseName: 'New Venture Creation',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 401',
                    courseName: 'Final Year Project I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VIII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'LAW 310',
                    courseName: 'Business Law',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CS 480',
                    courseName: 'Professionalism in Computing',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 402',
                    courseName: 'Final Year Project II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'DE',
                    courseName: 'Department Elective',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
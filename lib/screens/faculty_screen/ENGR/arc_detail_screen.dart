import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/info_screens/admission_requirement.dart';
import 'package:paragon_handbook/components/course_card.dart';

class ARCDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'What career pathways are open to graduates of the ARC program?',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 25.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.49,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Text(
                    'While possessing knowledge of current design trends and capabilities to produce unique modern designs, ARC students are also familiar with construction management and urban planning. This makes them attractive candidates for a variety of different positions. Commonly,  students are employed as architects or designers of different kinds. However, possible jobs are not limited by these two fields. Many ARC graduates choose to pursue careers in urban planning or historic conservation. Today, architects are solving problems, allowing for the creation of greener and more sustainable places for living.',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Generally, ARC graduates can expect to be  hired in the following fields:',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  BulletPoints(
                    text: 'Architecture (Residential/Commercial Architect, Landscape Architect, Restoration Architect, Architectural Technologist, Architectural Draftsman, Architectural Photographer)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Design (Industrial Designer, Interior Designer, Spatial Designer, Production Designer, Graphic Designer, 3D Visualizer)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Urban Planning (Town Planning Conceptual Architect, Urban Planner)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Historic Conservation (Conservation Officer, Building Surveyor, Historic Buildings Inspector)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Engineering (Structural Engineer)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Entrepreneurship (Architecture Company Owner)',
                  ),
                  SizedBox(height: 20.0,),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context) => AdmissionRequirement())
                      );
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width:  MediaQuery.of(context).size.width * 0.35,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(20.0)
                      ),
                      child: Center(
                        child: Text(
                          'Apply Now',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sample Curriculum',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 26.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER I',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 101',
                    courseName: 'Academic English I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 101',
                    courseName: 'Khmer Studies I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 130',
                    courseName: 'Pre-Calculus',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'SSC 101',
                    courseName: 'Introduction to Social Science',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 101',
                    courseName: 'Architectural Drawing Creativity',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER II',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 102',
                    courseName: 'Academic English II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 102',
                    courseName: 'Khmer Studies II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'PHYS 100',
                    courseName: 'Conceptual Physics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'SSC 112',
                    courseName: 'Human Geography',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 112',
                    courseName: 'Computer Design Architect I',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER III',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ARC 201',
                    courseName: 'Architectural Design I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 213',
                    courseName: 'Computer Design Architects II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 221',
                    courseName: 'Khmer Arts and Architecture',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 242',
                    courseName: 'Interior Architecture',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 231',
                    courseName: 'Building Science',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER IV',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ARC 202',
                    courseName: 'Architectural Design II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 246',
                    courseName: 'Landscape Architecture',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 222',
                    courseName: 'History of Art and Architecture I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CM 212',
                    courseName: 'Construction Technologies',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER V',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ARC 301',
                    courseName: 'Architectural Design III',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 331',
                    courseName: 'Structural Design Architecture I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 323',
                    courseName: 'History of Art and Architecture II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 341',
                    courseName: 'Sustainable Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 231',
                    courseName: 'Building Science',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VI',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ARC 302',
                    courseName: 'Architectural Design IV',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ENGR 462',
                    courseName: 'BIM Theory and Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 324',
                    courseName:  'History of Art and Architecture III',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 332',
                    courseName: 'Structural Design Architecture II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 312',
                    courseName: 'Building’s Systems (MEP)',
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ARC 401',
                    courseName: 'Architectural Design V',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 441',
                    courseName: 'Theory of Urban Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CM 431',
                    courseName: 'Project Management Engineering',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 451',
                    courseName: 'Internship',
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VIII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ARC 332',
                    courseName: 'Structural Design Architecture II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 402',
                    courseName: 'Capstone Architectural Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 442',
                    courseName: 'Professional Practice',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ARC 246',
                    courseName: 'Landscape Architecture',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'PIU 252',
                    courseName: 'Innovative Design Thinking',
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
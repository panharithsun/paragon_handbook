import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/faculty_screen/ENGR/arc_detail_screen.dart';
import 'package:paragon_handbook/screens/faculty_screen/ENGR/ce_detail_screen.dart';
import 'package:paragon_handbook/screens/faculty_screen/ENGR/ie_detail_screen.dart';
import 'package:paragon_handbook/components/department_info.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class EngrScreen extends StatelessWidget {

  final controller = PageController(initialPage: 0);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: Center(
        child: Stack(
          children: [
            PageView(
              controller: controller,
              children: <Widget>[
                DepartmentInformation(
                  topPadding: 42.0,
                  number: '01',
                  imgUrl: 'assets/images/engr_ce.jpg',
                  departmentName: 'D E P A R T M E N T  O F \nC I V I L  E N G I N E E R I N G',
                  departmentQuestion: 'What is the focus of the Civil Engineering \n(CE) program?',
                  description: 'Since the beginning of the 21st  century, Cambodia has been on a path of rapid economic development. This means the construction sector has been booming with opportunities. The development of the infrastructure, such as road and bridge construction, as well as the building of new apartment complexes and shopping malls, are trending in the Kingdom. Combined with the high interest from foreign investors, these developments make the civil engineering field very lucrative.\n\nCivil engineering includes designing and constructing facilities that are essential for human welfare. This includes buildings we live in, roads that we use for commuting, bridges for easy access, dams to produce energy, and many others. It is one of the earliest engineering disciplines that humanity has been practicing to develop civilization.',
                  function: () {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => CEDetailScreen())
                    );
                  },
                ),
                DepartmentInformation(
                  number: '02',
                  imgUrl: 'assets/images/engr_arc.jpg',
                  departmentName: 'D E P A R T M E N T  O F \nA R C H I T E C T U R E',
                  departmentQuestion: 'What is the focus of the Architecture \n(ARC) program?',
                  description: 'In recent years, Cambodia has been actively moving forward, which resulted not only in the rapid development of infrastructure but also in the alteration of people’s lifestyles and values. Inspired by examples of Bangkok, Kuala Lumpur, and Singapore, Cambodians demand from their cities not only functional but aesthetically pleasing designs. Local businesses do everything to match expectations by hiring architecture specialists to create appealing urban spaces. More and more office buildings, malls, and coffee shops adopt modern design to aesthetically satisfy customers. In the next few decades, Cambodian cities will continue to change the way they are organized and the way they look.',
                  function: () {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => ARCDetailScreen())
                    );
                  },
                ),
                DepartmentInformation(
                  scrollHeight: 0.42,
                  number: '03',
                  imgUrl: 'assets/images/engr_ie.jpg',
                  departmentName: 'D E P A R T M E N T  O F \nI N D U S T R I A L  E N G I N E E R I N G',
                  departmentQuestion: 'What is the focus of the Industrial Engineering \n(IE) program?',
                  description: 'In the past decade, Cambodia has made a few decisions leading towards Industry 4.0. In other words, it goes along with a global automation trend to solve sustainability, the labor market, and other problems. Most of the Industry 4.0 goals can be achieved by merging information and communication technologies and making the existing processes more efficient. In fact, efficiency is one of the key concerns of today’s production and manufacturing sectors, as it allows businesses to stay competitive. Therefore, Cambodia, like many other countries, is encouraging the human resource pool to upgrade their traditional competencies and skills.',
                  function: () {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => IEDetailScreen())
                    );
                  },
                ),
              ],
            ),
            Positioned(
              bottom: 5.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                height: 30.0,
                width: 20.0,
                child: Center(
                  child: SmoothPageIndicator(
                    controller: controller,
                    count: 3,
                    effect: WormEffect(
                      dotWidth: 10,
                      dotHeight: 10,
                      radius: 16,
                      dotColor: Colors.grey,
                      activeDotColor: Color(0xffFFAE00),
                    ),
                    onDotClicked: (int page) {
                      controller.animateToPage(
                        page,
                        duration: Duration(milliseconds: 400),
                        curve: Curves.ease,
                      );
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/info_screens/admission_requirement.dart';
import 'package:paragon_handbook/components/course_card.dart';

class CEDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'What career pathways are open to graduates of the CE program?',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 25.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.49,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Text(
                    'Upon graduation from ParagonIU, CE students qualify for responsible positions in the industry in analysis, design, and construction management. Prospective employers include, but are not limited to, construction contractors, consulting firms, industrial firms, and various government agencies.',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Therefore,  CE graduates can find themselves employed as:',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  BulletPoints(
                    text: 'Local and regional consultants (planning and designing projects, supervising the implementation of construction plans)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Local and regional contractors (organizing the execution of the designs on sites, overseeing labor forces and materials, considering time, cost, and safety precautions)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Local and regional authorities (being responsible for water, drainage, transport, and highway systems)',
                  ),
                  SizedBox(height: 20.0,),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context) => AdmissionRequirement())
                      );
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width:  MediaQuery.of(context).size.width * 0.35,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(20.0)
                      ),
                      child: Center(
                        child: Text(
                          'Apply Now',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sample Curriculum',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 26.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER I',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 101',
                    courseName: 'Academic English I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ECON 100',
                    courseName: 'Introductory Economics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 101',
                    courseName: 'Khmer Studies I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 131',
                    courseName: 'Calculus I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ENGR 101',
                    courseName: 'Introduction to Engineering',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'PHYS 131',
                    courseName: 'Physics I',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER II',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 102',
                    courseName: 'Academic English II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ECON 201',
                    courseName: 'Microeconomics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 102',
                    courseName: 'Khmer Studies II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 132',
                    courseName: 'Calculus II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CHEM 106',
                    courseName: 'General Chemistry',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'PHYS 132',
                    courseName: 'Physics II',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER III',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CE 211',
                    courseName: 'Engineering Geology',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ENGR 201',
                    courseName: 'Engineering Statics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ENGR 205',
                    courseName: 'Computer Geometric Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ENGR 231',
                    courseName: 'Introduction to Programming',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 233',
                    courseName: 'Calculus III',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 241',
                    courseName: 'Linear Algebra',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER IV',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CE 202',
                    courseName: 'Strength of Materials',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 212',
                    courseName: 'Construction Materials',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 252',
                    courseName: 'Differential Equations',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 250',
                    courseName: 'Probability and Statistics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ENGR 202',
                    courseName: 'Engineering Dynamics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'PHYS 233',
                    courseName: 'Electricity and Magnetism',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER V',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CE 301',
                    courseName: 'Structural Analysis I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 311',
                    courseName: 'Fluid Mechanics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 341',
                    courseName: 'Strength of Materials II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 321',
                    courseName: 'Surveying',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 331',
                    courseName: 'Soil Mechanics & Lab',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VI',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CE 302',
                    courseName: 'Structural Analysis',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 340',
                    courseName: 'Reinforced Concrete Structures',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 312',
                    courseName: 'Hydraulics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 332',
                    courseName: 'Foundation Engineering',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 350',
                    courseName: 'Structural Timber Design',
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CE 421',
                    courseName: 'Environmental Engineering',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 451',
                    courseName: 'Transportation Engineering',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 461',
                    courseName: 'Fundementals of Steel Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CM 431',
                    courseName: 'Project Management Engineering',
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VIII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'CE 492',
                    courseName: 'Project',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ENGR 462',
                    courseName: 'BIM Theory and Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 490',
                    courseName: 'Highway Design',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'CE 484',
                    courseName: 'Advanced Structural Analysis',
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/faculty_screen/ECON/baf_detail_screen.dart';
import 'package:paragon_handbook/screens/faculty_screen/ECON/bus_detail_screen.dart';
import 'package:paragon_handbook/screens/faculty_screen/ECON/ir_detail_screen.dart';
import 'package:paragon_handbook/screens/faculty_screen/ECON/itl_detail_screen.dart';
import 'package:paragon_handbook/components/department_info.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class EconScreen extends StatelessWidget {

  final controller = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: Center(
        child: Stack(
          children: [
            PageView(
              controller: controller,
              children: <Widget> [
                DepartmentInformation(
                  number: '01',
                  imgUrl: 'assets/images/econ_bus.jpg',
                  departmentName: 'D E P A R T M E N T  O F \nB U S I N E S S  A D M I N I S T R A T I O N',
                  departmentQuestion: 'What is the focus of the Business Administration (BUS) program?',
                  description: 'Since the beginning of the century, Cambodia has been pursuing the ambition of attaining an upper-middle-income status by 2030. In order to do this, it actively works on supporting small and medium enterprises (SMEs). According to some government officials, modernizing SMEs is a crucial task for the Cambodian economy if it wants to compete globally. That is why the Royal Government of Cambodia actively encourages new business initiatives through investment and otherwise. In relation to this, studying business administration is very popular among Cambodian students. \n\nAdditionally, in recent years many international companies have chosen to expand to the Cambodian market. These companies choose to employ qualified young professionals with developed soft skills and knowledge of business administration. However, there is still a visible shortage of such individuals in Cambodia today.',
                  function: () {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => BUSDetailScreen())
                    );
                  },
                ),
                DepartmentInformation(
                  number: '02',
                  imgUrl: 'assets/images/econ_baf.jpg',
                  departmentName: 'D E P A R T M E N T  O F \nB A N K I N G  A N D  F I N A N C E',
                  departmentQuestion: 'What is the focus of the Banking and Finance \n(BAF) program?',
                  description: 'In recent years, Cambodia has been experiencing a significant leap in development with GDP growth being among the highest in Asia. At the same time, the Royal Government of Cambodia has been actively developing ties with ASEAN, and recently the Kingdom became a highly attractive market for foreign direct investment. All of these gave rise to financial activity increasing the need for quality online and offline financial services. Together with the takeover by traditional banks, Cambodia is also looking at the rapid development of the fintech industry. For the past few years, the Kingdom has seen a spark of highly successful financial management startups, such as Wing, TrueMoney, Bongloy, and ventures that are ready to invest millions in them.\n\nAll this means that the Cambodian banking sector is experiencing a strong need for qualified employees and financial experts. The Department of Banking and Finance aims to raise a new generation of high-potential professionals and therefore contribute to the labor market’s needs. For years, our department has successfully cooperated with leaders in the Cambodian banking industry in terms of curriculum development and internship opportunities. For many years, the BAF department remains of high interest to Cambodian high-school graduates.',
                  function: () {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => BAFDetailScreen())
                    );
                  },
                ),
                DepartmentInformation(
                  topPadding: 20,
                  number: '03',
                  imgUrl: 'assets/images/econ_itl1.jpg',
                  departmentName: 'D E P A R T M E N T  O F \nI N T E R N A T I O N A L  T R A D E  \nA N D  L O G I S T I C ',
                  departmentQuestion: 'What is the focus of the International Trade and \nLogistics (ITL) program?',
                  description: 'In recent years, Cambodia has been experiencing a significant leap in development with GDP growth being among the highest in Asia. At the same time, the Royal Government of Cambodia has been actively developing ties with ASEAN, and recently the Kingdom became a highly attractive market for foreign direct investment. All of these gave rise to financial activity increasing the need for quality online and offline financial services. Together with the takeover by traditional banks, Cambodia is also looking at the rapid development of the fintech industry. For the past few years, the Kingdom has seen a spark of highly successful financial management startups, such as Wing, TrueMoney, Bongloy, and ventures that are ready to invest millions in them.\n\nAll this means that the Cambodian banking sector is experiencing a strong need for qualified employees and financial experts. The Department of Banking and Finance aims to raise a new generation of high-potential professionals and therefore contribute to the labor market’s needs. For years, our department has successfully cooperated with leaders in the Cambodian banking industry in terms of curriculum development and internship opportunities. For many years, the BAF department remains of high interest to Cambodian high-school graduates.',
                  top: 150,
                  function: () {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => ITLDetailScreen())
                    );
                  },
                ),
                DepartmentInformation(
                  topPadding: 20,
                  number: '04',
                  imgUrl: 'assets/images/econ_ir.jpg',
                  departmentName: 'D E P A R T M E N T  O F \nP O L I T I C A L  S C I E N C E  A N D\nI N T E R N A T I O N A L  R E L A T I O N S ',
                  departmentQuestion: 'What is the focus of the Political Science and \nInternational Relations (PS&IR) program?',
                  description: 'The Political Science and International Relations students at ParagonIU learn about the theories and issues in current international affairs so that they can actively participate in shaping the direction and betterment of Cambodia and the world. The program values open-mindedness, critical thinking, a sense of community, and competent leadership. Throughout the program, students get exposed to various theories and “Schools of Thought” in international relations and political science, and this helps them make sense of the complexities of national and world affairs.',
                  top: 150,
                  function: () {
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => IRDetailScreen())
                    );
                  },
                ),
              ],
            ),
            Positioned(
              bottom: 5.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                height: 30.0,
                width: 20.0,
                child: Center(
                  child: SmoothPageIndicator(
                    controller: controller,
                    count: 4,
                    effect: WormEffect(
                      dotWidth: 10,
                      dotHeight: 10,
                      radius: 16,
                      dotColor: Colors.grey,
                      activeDotColor: Color(0xffFFAE00),
                    ),
                    onDotClicked: (int page) {
                      controller.animateToPage(
                        page,
                        duration: Duration(milliseconds: 400),
                        curve: Curves.ease,
                      );
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}


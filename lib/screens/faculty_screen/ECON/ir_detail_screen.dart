import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';
import 'package:paragon_handbook/screens/admissioninfo_screen/info_screens/admission_requirement.dart';
import 'package:paragon_handbook/components/course_card.dart';

class IRDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'What career pathways are open to graduates of the PS&IR program?',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 25.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.49,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Text(
                    'The IR graduates are the most suitable candidates for careers in the diplomatic corps, foreign affairs ministry, and international affairs offices in both governmental and non-governmental organizations. Additionally, they might find employment at multinational corporations, UN and international development agencies. Graduates of the program may also continue to study on graduate programs abroad if they wish to join academia and research community (i.e. teaching at universities or working as researchers for think-tanks and institutes).',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Some of the potential organizations and fields of employment are:',  
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 20),
                  BulletPoints(
                    text: 'Embassies and Consulates (Ambassador, Consul, Embassy Employee)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Ministry of Foreign Affairs (MoFA Officer)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Think Tanks (Research Fellow, Executive positions)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Universities (Researcher, Lecturer)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'NGOs (NGO Manager)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Businesses (International Business Manager)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'United Nations and other international organizations (Employee at different UN Departments or other international organizations)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Foreign Services (Foreign Services Officer, Foreign Correspondent)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Intelligence (Intelligence Officer)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Public Affairs (Public Affairs Specialist)',
                  ),
                  SizedBox(height: 15.0,),
                  BulletPoints(
                    text: 'Foreign Investment (Foreign Investment Specialist)',
                  ),
                  SizedBox(height: 20.0,),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context) => AdmissionRequirement())
                      );
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width:  MediaQuery.of(context).size.width * 0.35,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(20.0)
                      ),
                      child: Center(
                        child: Text(
                          'Apply Now',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sample Curriculum',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 26.0,
                      color: Color(0xff192f59)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER I',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 101',
                    courseName: 'Academic English I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 101',
                    courseName: 'Khmer Studies I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ECON 100',
                    courseName: 'Introductory Economics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MIS 110',
                    courseName: 'Introduction to IT Applications',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'HIST 101',
                    courseName: 'World History and Civilizations I',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 101',
                    courseName: 'IR Orientation I',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER II',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ENGL 102',
                    courseName: 'Academic English II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'KHM 102',
                    courseName: 'Khmer Studies II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'MATH 111',
                    courseName: 'Essentials of Mathematics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'ECON 202',
                    courseName: 'Macroeconomics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'HIST 102',
                    courseName: 'World History and Civilizations II',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 102',
                    courseName:  'IR Orientation II',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER III',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ECON 211',
                    courseName: 'International Economics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 201',
                    courseName: 'Introduction to Political Science',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 221',
                    courseName: 'Nation-States & Comparative Pol',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'SOC 211',
                    courseName: 'Introduction to Sociology',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 211',
                    courseName: 'Cultures and Societies in SEA',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER IV',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'ECON 212',
                    courseName: 'International Finance for Dev',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 202',
                    courseName: 'Introduction to IR',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 220',
                    courseName: 'ASEAN in the Regional & Global ',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 230',
                    courseName: 'Non-State Actors in World Politics',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'STAT 211',
                    courseName: 'Statistics for Social Sciences',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER V',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'IR 301',
                    courseName: 'Comparative Foreign Policy',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 320',
                    courseName: 'Public Policy',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'LAW 301',
                    courseName: 'Introduction to International Law',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 320',
                    courseName: 'Public Policy',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'RES 301',
                    courseName: 'Research Methods in SS',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 321',
                    courseName: 'History of Political Thought',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'GE',
                    courseName: 'General Elective',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VI',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'IR 330',
                    courseName: 'International Political Economy',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 302',
                    courseName: 'International Relations of East Asia',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'LAW 302',
                    courseName:  'Institutional Law of Int Org',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 350',
                    courseName: 'Media in Domestic and Global Pol',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 340',
                    courseName: 'Pre-Thesis Seminar',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'IR 401',
                    courseName: 'Contemporary Theories Global Pol',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 460',
                    courseName: 'Seminar on Trans-boundary',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 422',
                    courseName: 'Issues in Post-Conflict Societies',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'LAW 401',
                    courseName: 'Comparative Law',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 441',
                    courseName: 'Senior Thesis I',
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffFFAE00),
              child: Text(
                'SEMESTER VIII',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CourseCard(
                    courseCode: 'IR 412',
                    courseName: 'Cambodia’s Foreign Policy',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 450',
                    courseName: 'Government and Politics in SEA',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 421',
                    courseName: 'Issues In Development Studies',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 402',
                    courseName: 'Special Topics in Politics & IR',
                  ),
                  SizedBox(height: 10.0,),
                  Divider(color: Colors.black38,),
                  SizedBox(height: 10.0,),
                  CourseCard(
                    courseCode: 'IR 442',
                    courseName: 'Senior Thesis II',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/faculty_screen/faculty_screen.dart';
import 'package:paragon_handbook/components/hamburgerMenu.dart';

class StackScreenFaculty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        HamburgerMenu(),
        FacultyScreen(),
      ],
      ),
    );
  }
}
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:paragon_handbook/screens/faculty_screen/ECON/econ.dart';
import 'package:paragon_handbook/screens/faculty_screen/ENGR/engr.dart';
import 'package:paragon_handbook/screens/faculty_screen/ITC/ict.dart';

class FacultyScreen extends StatefulWidget {
  FacultyScreen({Key key}) : super(key: key);

  @override
  _FacultyScreenState createState() => _FacultyScreenState();
}

class _FacultyScreenState extends State<FacultyScreen> {
  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;
  bool isDrawerOpen = false;
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(xOffset, yOffset, 0)
        ..scale(scaleFactor),
      duration: Duration(milliseconds: 200),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          toolbarHeight: 60,
          backgroundColor: Colors.white,
          title: Row(
            children: [
              isDrawerOpen
                  ? IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Color(0xff192f59),
                      ),
                      onPressed: () {
                        setState(() {
                          xOffset = 0;
                          yOffset = 0;
                          scaleFactor = 1;
                          isDrawerOpen = false;
                        });
                      },
                    )
                  : Container(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            xOffset = 200;
                            yOffset = 100;
                            scaleFactor = 0.7;
                            isDrawerOpen = true;
                          });
                        },
                        child: SvgPicture.asset(
                          'assets/svgs/menu.svg',
                          height: 15.0,
                          color: Color(0xff192f59),
                        ),
                      ),
                    ),
              SizedBox(
                width: 25.0,
              ),
              Image.asset(
                'assets/images/logo_with_name_blue.png',
                height: 40.0,
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Positioned(
                    child: Container(
                      height: 500,
                      width: 500,
                    ),
                  ),
                  Positioned(
                    left: 20,
                   child: Container(
                        child: TyperAnimatedTextKit(
                          text: ["U N I V E R S I T Y  \nF A C U L T I E S"],
                          textStyle: TextStyle(
                            fontFamily: 'AvenirNext',
                            fontSize: 30.0,
                            fontWeight: FontWeight.w400,
                          ),)
                      ),
                  ),
                  Positioned(
                    top: 120,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 125,
                      child: Image.asset(
                        'assets/images/ict.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 120,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => ICTpage()));
                      },
                      child: Opacity(
                        opacity: 0.5,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          color: Color(0xff192f59),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 50,
                    top: 155,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => ICTpage()));
                      },
                      child: Text('Information and\nComputer Technologies',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          )),
                    ),
                  ),
                  Positioned(
                    top: 245,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 125,
                      child: Image.asset(
                        'assets/images/engr.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 245,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EngrScreen()));
                      },
                      child: Opacity(
                        opacity: 0.5,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          color: Color(0xff192f59),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 50,
                    top: 292,
                    child: GestureDetector(
                        onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EngrScreen()));
                      },
                      child: Text('Engineering',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          )),
                    ),
                  ),
                  Positioned(
                    top: 370,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 125,
                      child: Image.asset(
                        'assets/images/econ.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 370,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EconScreen()));
                      },
                      child: Opacity(
                        opacity: 0.5,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          color: Color(0xff192f59),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 50,
                    top: 405,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EconScreen()));
                      },
                      child: Text('Economics and\nAdministrative Sciences',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          )),
                    ),
                  ),
                  Positioned(
                    left: 16.5,
                    top: 175,
                    child: Container(
                      height: 20,
                      width: 20,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 16.5,
                    top: 300,
                    child: Container(
                      height: 20,
                      width: 20,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 16.5,
                    top: 425,
                    child: Container(
                      height: 20,
                      width: 20,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 22.5,
                    top: 80,
                    child: Container(
                      height: 550,
                      width: 7.5,
                      decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                ],
                overflow: Overflow.visible,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

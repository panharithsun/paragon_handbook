import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:page_transition/page_transition.dart';
import 'package:paragon_handbook/screens/home_screen/stackScreenHomePage.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: Container(
          child: Image.asset(
        'assets/images/logo.png',
        height: 500.0,
      )),
      nextScreen: StackScreenHomePage(),
      splashTransition: SplashTransition.fadeTransition,
      pageTransitionType: PageTransitionType.fade,
      duration: 1000,
      backgroundColor: Color(0xff192F59),
    );
  }
}

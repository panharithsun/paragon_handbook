import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/hamburgerMenu.dart';
import 'package:paragon_handbook/screens/school_event/school_event_screen.dart';

class StackScreenSchoolEvent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        HamburgerMenu(),
        SchoolEventScreen(),
      ],
      ),
    );
  }
}
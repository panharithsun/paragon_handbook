import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'detail_school_event.dart';

class SchoolEventScreen extends StatefulWidget {
  SchoolEventScreen({Key key}) : super(key: key);

  @override
  _SchoolEventScreenState createState() => _SchoolEventScreenState();
}

class _SchoolEventScreenState extends State<SchoolEventScreen> {

  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;

  bool isDrawerOpen = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(xOffset, yOffset, 0)
        ..scale(scaleFactor),
      duration: Duration(milliseconds: 200),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            toolbarHeight: 60,
            backgroundColor: Color(0xffffffff),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                isDrawerOpen  
                ?  IconButton(
                    icon: Icon(Icons.arrow_back_ios, color: Color(0xff192f59),),
                    onPressed: () {
                      setState(() {
                        xOffset = 0;
                        yOffset = 0;
                        scaleFactor = 1;
                        isDrawerOpen = false;
                      });
                    },
                  )
                : Container(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        xOffset = 200;
                        yOffset = 100;
                        scaleFactor = 0.7;
                        isDrawerOpen = true;
                      });
                    },
                    child: SvgPicture.asset(
                      'assets/svgs/menu.svg',
                      height: 15.0,
                      color: Color(0xff192f59),
                    ),
                  ),
                ),
                SizedBox(
                  width: 25.0,
                ),
                Image.asset(
                  'assets/images/logo_with_name_blue.png',
                  height: 40.0,
                )
              ],
            ),
          ),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Positioned(
                      child: Container(
                        height: 800,
                        width: 500,
                      ),
                    ),
                    Positioned(
                      top: 20,
                      left: 20,
                      child: Container(
                        child: TyperAnimatedTextKit(
                          text: ["S C H O O L'S  \nE V E N T S"],
                          textStyle: TextStyle(
                            fontFamily: 'AvenirNext',
                            fontSize: 30.0,
                            fontWeight: FontWeight.w400,
                          ),)
                      ),
                    ),
                    Positioned(
                      //photo
                      top: 120,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 125,
                        child: Image.asset(
                          'assets/images/ict.png',
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Positioned(
                      //opacity of blue container
                      top: 120,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Opacity(
                          opacity: 0.5,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 125,
                            color: Color(0xff192f59),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      //text
                      left: 50,
                      top: 170,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Text('Cambodia CS Cup',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          )
                        ),
                      ),
                    ),
                    Positioned(
                      top: 245,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 125,
                        child: Image.asset(
                          'assets/images/event_debate.jpg',
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 245,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Opacity(
                          opacity: 0.5,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 125,
                            color: Color(0xff192f59),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 50,
                      top: 280,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Text('ParagonIU\nDebate Competition',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w500,
                            )),
                      ),
                    ),
                    Positioned(
                      top: 370,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 125,
                        child: Image.asset(
                          'assets/images/event_ycc.jpg',
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 370,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Opacity(
                          opacity: 0.5,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 125,
                            color: Color(0xff192f59),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 50,
                      top: 420,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Text('You Can Code(YCC)',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          )
                        ),
                      ),
                    ),
                    Positioned(
                      //photo
                      top: 495,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 125,
                        child: Image.asset(
                          'assets/images/event_table_tennis.jpg',
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Positioned(
                      //opacity of blue container
                      top: 495,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Opacity(
                          opacity: 0.5,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 125,
                            color: Color(0xff192f59),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      //text
                      left: 50,
                      top: 542,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Text('PIU Table Tennis Tournament',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          )
                        ),
                      ),
                    ),
                    Positioned(
                      //photo
                      top: 620,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 125,
                        child: Image.asset(
                          'assets/images/event_hackathon.jpg',
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Positioned(
                      //opacity of blue container
                      top: 620,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Opacity(
                          opacity: 0.5,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 125,
                            color: Color(0xff192f59),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      //text
                      left: 50,
                      top: 670,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=>DetailSchoolEvents()));
                        },
                        child: Text('PIU Hackathon',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w600,
                          )
                        ),
                      ),
                    ),
                    Positioned(
                      left: 16.5,
                      top: 175,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: Color(0xffFFAE00),
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 16.5,
                      top: 300,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: Color(0xffFFAE00),
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 16.5,
                      top: 425,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: Color(0xffFFAE00),
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 16.5,
                      top: 550,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: Color(0xffFFAE00),
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 16.5,
                      top: 675,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: Color(0xffFFAE00),
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                    //straight line
                    Positioned(
                      left: 22.5,
                      top: 100,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 2,
                        width: 7.5,
                        decoration: BoxDecoration(
                          color: Color(0xffFFAE00),
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                  ],
                  overflow: Overflow.visible,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

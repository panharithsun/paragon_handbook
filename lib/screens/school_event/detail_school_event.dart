import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailSchoolEvents extends StatefulWidget {
  @override
  _DetailSchoolEventsState createState() => _DetailSchoolEventsState();
}

class _DetailSchoolEventsState extends State<DetailSchoolEvents> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Positioned(
                  child: Container(
                      child: Row(
                    children: [
                      Padding(padding: EdgeInsets.only(left: 20.0)),
                      Container(
                        width: 100,
                        height: 3200,
                        color: Color(0xffFFAE00),
                      ),
                    ],
                  )),
                ),
                Positioned(
                  left: 150.0,
                  top: 150.0,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 150.0,
                        ),
                        Text(
                          "Cambodia CS Cup\n",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Cambodia dog Cup is an annual competitive programming competition organized by several Cambodian universities where high school and university students compete against each other as individuals rather than in teams.",
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 150.0,
                  top: 730.0,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 150.0,
                        ),
                        Text(
                          "ParagonIU Debate Competition\n",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "The Office of Student Services and ParagonIU Debate Club are pleased to announce that there will be a debate competition in February. ParagonIU Debate 2020 is an Asian Parliamentary debate that allows contestants to prepare themselves for both national and international debate tournaments.",
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 150.0,
                  top: 1350.0,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 150.0,
                        ),
                        Text(
                          "Paragon IU Hackathon\n",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "On Hackathon day, each team will be given the coding challenge to build a web application using Facebook API. There are tech mentors in command of guiding you to build a successful project. ",
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 150.0,
                  top: 1900.0,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 150.0,
                        ),
                        Text(
                          "PIU Table Tennis Tournament\n",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "We are pleased to inform you that the Paragon International University Table Tennis Tournament is back again with a more challenging experience and rewards. Your ability will be shown in this tournament with appreciation and we would love to see your skills in the table tennis at the university.",
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 150.0,
                  top: 2500.0,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 150.0,
                        ),
                        Text(
                          "You Can Code (YCC)\n",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "YCC (You Can Code) is the first event created by the ICT Faculty from Paragon International University designed to give High School Students more understanding of the ICT Field and at the same time provide them with the idea of how to self-learn Programming Skill. During this event, High School Student will be able to understand the foundation of basic programming skills and have the ability to build their first website and webpage. ",
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                Picture(
                  imageLink: 'assets/images/CS_Cup.jpg',
                  toplink: 40,
                ),
                Picture(
                  imageLink: 'assets/images/PIU_Debate_competition.jpg',
                  toplink: 600.0,
                ),
                Picture(
                  imageLink: 'assets/images/PIU_Hackathon.jpg',
                  toplink: 1250.0,
                ),
                Picture(
                  imageLink: 'assets/images/Table_Tennis.jpg',
                  toplink: 1800.0,
                ),
                Picture(
                  imageLink: 'assets/images/You_Can_Code.jpg',
                  toplink: 2400.0,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class Picture extends StatelessWidget {
  final String imageLink;
  final double toplink;
  Picture({this.imageLink, this.toplink});

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: toplink,
        left: 50.0,
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 11,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Image.asset(
            imageLink,
            height: 220.0,
          ),
        ));
  }
}

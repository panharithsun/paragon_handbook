import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:paragon_handbook/screens/uni_service/info_screen/student_council.dart';
import 'package:paragon_handbook/screens/uni_service/info_screen/studentsclub.dart';
import 'package:paragon_handbook/screens/uni_service/info_screen/student_services.dart';
import 'package:paragon_handbook/screens/uni_service/info_screen/university_library.dart';

class UniServiceScreen extends StatefulWidget {
  @override
  _UniServiceScreenState createState() => _UniServiceScreenState();
}

class _UniServiceScreenState extends State<UniServiceScreen> {
  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;

  bool isDrawerOpen = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        transform: Matrix4.translationValues(xOffset, yOffset, 0)
          ..scale(scaleFactor),
        duration: Duration(milliseconds: 200),
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            toolbarHeight: 60,
            backgroundColor: Color(0xffffffff),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                isDrawerOpen
                    ? IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Color(0xff192f59),
                        ),
                        onPressed: () {
                          setState(() {
                            xOffset = 0;
                            yOffset = 0;
                            scaleFactor = 1;
                            isDrawerOpen = false;
                          });
                        },
                      )
                    : Container(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              xOffset = 200;
                              yOffset = 100;
                              scaleFactor = 0.7;
                              isDrawerOpen = true;
                            });
                          },
                          child: SvgPicture.asset(
                            'assets/svgs/menu.svg',
                            height: 15.0,
                            color: Color(0xff192f59),
                          ),
                        ),
                      ),
                SizedBox(
                  width: 25.0,
                ),
                Image.asset(
                  'assets/images/logo_with_name_blue.png',
                  height: 40.0,
                )
              ],
            ),
          ),
          body: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Positioned(
                        child: Container(
                          height: 700,
                          width: 700,
                        ),
                      ),
                      Positioned(
                        top: 50,
                        left: 20,
                        child: Container(
                        child: TyperAnimatedTextKit(
                          text: ["U N I V E R S I T Y  \nS E R V I C E S"],
                          textStyle: TextStyle(
                            fontFamily: 'AvenirNext',
                            fontSize: 30.0,
                            fontWeight: FontWeight.w400,
                          ),)
                      ),
                      ),
                      Positioned(
                        top: 170,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          child: Image.asset(
                            'assets/images/uniservice_library.jpg',
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                      Positioned(
                        top: 170,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UniversityLibrary()));
                          },
                          child: Opacity(
                            opacity: 0.5,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 125,
                              color: Color(0xff192f59),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 50,
                        top: 220,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UniversityLibrary()));
                          },
                          child: Text('University Library',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              )),
                        ),
                      ),
                      Positioned(
                        top: 295,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          child: Image.asset(
                            'assets/images/uniservice_student_service.jpg',
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                      Positioned(
                        top: 295,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => StudentServices()));
                          },
                          child: Opacity(
                            opacity: 0.5,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 125,
                              color: Color(0xff192f59),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 50,
                        top: 342,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => StudentServices()));
                          },
                          child: Text('Student Services',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              )),
                        ),
                      ),
                      Positioned(
                        top: 420,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          child: Image.asset(
                            'assets/images/uniservice_student_activity.jpg',
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                      Positioned(
                        top: 420,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => StudentsClub()),
                            );
                          },
                          child: Opacity(
                            opacity: 0.5,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 125,
                              color: Color(0xff192f59),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 50,
                        top: 455,
                        child: GestureDetector(
                           onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => StudentsClub()),
                            );
                          },
                          child: Text(
                              'Student Clubs and\nCultural Activities Center',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              )),
                        ),
                      ),
                      Positioned(
                        top: 545,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 125,
                          child: Image.asset(
                            'assets/images/uniservice_student_council.jpg',
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                      Positioned(
                        top: 545,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Studentcouncil()),
                            );
                          },
                          child: Opacity(
                            opacity: 0.5,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 125,
                              color: Color(0xff192f59),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 50,
                        top: 575,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Studentcouncil()),
                            );
                          },
                          child: Text(
                              'Student Clubs and\nCultural Activities Center',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              )),
                        ),
                      ),
                      Positioned(
                        left: 16.5,
                        top: 225,
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            color: Color(0xffFFAE00),
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 16.5,
                        top: 350,
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            color: Color(0xffFFAE00),
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 16.5,
                        top: 475,
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            color: Color(0xffFFAE00),
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 16.5,
                        top: 595,
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            color: Color(0xffFFAE00),
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 22.5,
                        top: 130,
                        child: Container(
                          height: 590,
                          width: 7.5,
                          decoration: BoxDecoration(
                            color: Color(0xffFFAE00),
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                      ),
                    ],
                    overflow: Overflow.visible,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

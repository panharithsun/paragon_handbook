import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StudentsClub extends StatefulWidget {
  StudentsClub({Key key}) : super(key: key);

  @override
  _StudentsClubState createState() => _StudentsClubState();
}

class _StudentsClubState extends State<StudentsClub> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Student Clubs and Cultural Activities',
                      style: TextStyle(
                          fontSize: 22.0, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.57,
                      height: 5.0,
                      decoration: BoxDecoration(
                          color: Color(0xffFFAE00),
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      'This center helps students to organize and run their own cultural activities and clubs in which most students like to participate. We encourage students to join extracurricular activities because by joining students can enhance their overall educational experience at Paragon International University.',
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    )
                  ],
                )),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'The process of applying to create a student club',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                      'Each academic year, Paragon International University has numerous clubs (Khmer Traditional Dance and Cultural Club, Paragon International Model United Nation, International Student Club, Football Club, Aikido Club, Basketball Club, Table Tennis Club, Guitar Club, Paragon Youth Club, Robotic Club, Debate Club, etc) provided to students which enable them to organize and run their own activities based on their own interests. Every year we receive inquires about organizing clubs. Therefore, we have made the process easy for the creation of new clubs so that students will have the opportunity to participate in and enjoy additional extracurricular activities.'),
                ],
              )
            ),
            SizedBox(
              height: 20,
            ),
            Stack(
              children: <Widget>[
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.width * 0.42,
                            width: MediaQuery.of(context).size.width * 0.42,
                            child: Image.asset(
                              'assets/images/bookworm.jpg',
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.width * 0.42,
                            width: MediaQuery.of(context).size.width * 0.42,
                            child: Image.asset(
                              'assets/images/music.jpg',
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.width * 0.42,
                            width: MediaQuery.of(context).size.width * 0.42,
                            child: Image.asset(
                              'assets/images/soccer.jpg',
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.width * 0.42,
                            width: MediaQuery.of(context).size.width * 0.42,
                            child: Image.asset(
                              'assets/images/akido.jpg',
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 35,
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

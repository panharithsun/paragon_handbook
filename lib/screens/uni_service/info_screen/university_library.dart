import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/screens/uni_service/components/how_to_become_member.dart';
import 'package:paragon_handbook/screens/uni_service/components/membership_packages.dart';
import 'package:paragon_handbook/screens/uni_service/components/visit_website.dart';

class UniversityLibrary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
                child: Text(
                  'Paragon International University Library is open to the public',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                )),
            HowToBecomeAMember(),
            MembershipPackage(),
            VisitWebsite()
          ],
        ),
      ),
    );
  }
}

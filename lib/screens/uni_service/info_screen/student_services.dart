import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';

class StudentServices extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'The Office of Student Services',
                    style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.57,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Text(
                    'The Office of Student Services (OSS) caters to those who further the mission and curriculum of Paragon International University. This is accomplished with academic counseling, support, and discipline to promote an ethos of intellectual freedom based on mutual respect, integrity, and solid expectations of manifold achievement—academic, spiritual, as well as professional. Support services are delivered with a view to enhance the work productivity of students, faculty, administrators, and staff through programs that further loyalty, commitment, and a sense of community. To serve its users, the office often works hand in hand with students, staff and faculty, external organizations, professional and educational bodies.',
                    style: TextStyle(
                      fontSize: 16.0,
                    ),
                  )
                ],
              )
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'There are 5 Units in The Office of Student Services:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  BulletPoints(
                    text: 'Academic Support Unit' ,
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Student Council' ,
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Student Club and Cultural Activities' ,
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Career Center' ,
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Alumni Association' ,
                  ),
                ],
              )
            ),
            
            StudentServicesCard(
              imageUrl: 'assets/images/stuser_academic_support.png',
              name: 'Academic Support Unit',
              description: 'The Academic Support Unit aims to help students who are having problems with their learning processes. This may become apparent if they have been graded as unsatisfactory on an assessment, if one or more lecturers have indicated they are having problems, or if the student themselves feels that they are not making the progress which they feel they ought to be making.\n\nIf you come across a student who you believe is experiencing difficulties in their learning, please refer the student to the Academic Support Unit where they can gain the support they require.',
              lineLength: 0.52,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Essentials of the Academic Support Unit are:',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    )
                  ),
                  SizedBox(height: 20.0,),
                  BulletPoints(
                    text: 'Peer Tutoring',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Counselling Services',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'The Academic Success Workshop',
                  ),
                ],
              ),
            ),
            StudentServicesCard(
              imageUrl: 'assets/images/stuser_career_center.png',
              name: 'Career Center',
              description: 'The Career Center (CC) strives to provide the highest quality of comprehensive career services to all matriculated undergraduate students and alumni of Paragon International University. The Career Center functions as a vital component in the total educational experience of students, primarily in the development and implementation of career and educational plans. Career Services fosters partnerships with employers, alumni, faculty, staff, administrators, and the greater community to increase participation in providing opportunities for the career development of students.',
              lineLength: 0.32,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Essential Functions in the Career Center',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    )
                  ),
                  SizedBox(height: 20.0,),
                  BulletPoints(
                    text: 'Planning and counseling',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Career fairs and events',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Jobs and internships',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Interview and preparation',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Resume resources',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Career workshops',
                  ),
                ],
              ),
            ),
            StudentServicesCard(
              imageUrl: 'assets/images/stuser_student_council.png',
              name: 'Student Council',
              description: 'Student Council is a group of elected and volunteer students working together with an adult advisor within the framework of a constitution or bylaws to provide a means for student expression and assistance in school affairs and activities, give opportunities for student experience in leadership and encourage student, faculty and community relations.',
              lineLength: 0.36,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Through projects and activities, student councils work to:',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600
                    )
                  ),
                  SizedBox(height: 20.0,),
                  BulletPoints(
                    text: 'Promote Citizenship',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Promote Leadership',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Promote Cultural Values',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Promote Scholarship',
                  ),
                  SizedBox(height: 10.0,),
                  BulletPoints(
                    text: 'Promote Relation',
                  ),
                ],
              ),
            ),
            StudentServicesCard(
              imageUrl: 'assets/images/stuser_student_club.png',
              name: 'Student Club and \nCultural Activities',
              description: 'Each academic year, Paragon International University has numerous clubs (Khmer Traditional Dance and Cultural Club, Paragon International Model United Nation, International Student Club, Football Club, Aikido Club, Basketball Club, Table Tennis Club, Guitar Club, Paragon Youth Club, Robotic Club, Debate Club, etc) provided to students which enable them to organize and run their own activities based on their own interest.\n\nAnd each year, we received various reports about organizing the clubs which motivate us to ease in the registration for the creation of those clubs in order for students to have the encouragement of joining extracurricular activities.',
              lineLength: 0.4,
            ),
          ],
        ),
      ),
    );
  }
}

class StudentServicesCard extends StatelessWidget {

  final String imageUrl;
  final String name;
  final String description;
  final double lineLength;

  const StudentServicesCard({
    Key key, this.imageUrl, this.name, this.description, this.lineLength
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Image.asset(
                imageUrl,
                scale: 1.5,
              ),
              SizedBox(width: 20.0,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: TextStyle(
                      color: Color(0xff192f59),
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width * lineLength,
                    height: 5.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 20.0,),
          Text(
            description,
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
        ],
      ),
    );
  }
}
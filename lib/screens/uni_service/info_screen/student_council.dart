import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/bullet_point.dart';

class Studentcouncil extends StatefulWidget {
  Studentcouncil({Key key}) : super(key: key);

  @override
  _StudentcouncilState createState() => _StudentcouncilState();
}

class _StudentcouncilState extends State<Studentcouncil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 60,
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Color(0xff192f59),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Image.asset(
          'assets/images/logo_with_name_blue.png',
          height: 40.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Student Councils',
                    style: TextStyle(
                        fontSize: 22.0, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.57,
                    height: 5.0,
                    decoration: BoxDecoration(
                        color: Color(0xffFFAE00),
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Row(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.width * 0.2,
                        // width: MediaQuery.of(context).size.width * 0.2,
                        child: Image.asset(
                            'assets/images/stuser_student_council.png', height: 200),
                      ),
                      SizedBox(width: 20.0,),
                      Container(
                        width: ( MediaQuery.of(context).size.width * 0.9 ) - 100,
                        child: Text(
                          'This council is the representation of the students in the university so that they can be involved in school affairs by working in partnership with the University Management, Staff and Instructors for the advantage of the school and the students. The councils are carefully selected by a voting process from the students of Paragon International University and the representatives with the most votes become the student councils of the university.',
                          style: TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              )
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
              color: Color(0xffEFEFEF),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        'Mission',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16.0),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        height: 3.5,
                        width: 200.0,
                        decoration: BoxDecoration(
                            color: Color(0xffFFAE00),
                            borderRadius: BorderRadius.circular(100)),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                      'A Student Council is a representative structure through which students in a university can become involved in the affairs of the school, working in partnership with University Management, Staff and Instructors for the benefit of the school and its students. The main role of the Council as set out in the Constitution and By-Laws is “to promote the interests of the University and the involvement of students in the affairs of the school, in cooperation with the Board, Instructors and Staff”. A Student Council will set its own objectives, which will vary from school to school. Some general objectives could include:'),
                  SizedBox(
                    height: 20.0,
                  ),
                  BulletPoints(
                    text:
                        'To enhance communication between Students, Management, Staff and Instructors',
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  BulletPoints(
                    text:
                        'To promote an environment conducive to educational and personal development',
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  BulletPoints(
                    text: 'To promote friendship and respect among students',
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  BulletPoints(
                    text:
                        'To support Management and Staff in the development of the School',
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  BulletPoints(
                    text:
                        'To represent the views of the Students on matters of general concern to them',
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                      'A Student Council will identify activities that it would like to be involved in organizing, although the final decision on the activities of a Student Council should be agreed upon with School Management.'),
                ],
              )
            ),
          ]
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/hamburgerMenu.dart';
import 'package:paragon_handbook/screens/uni_service/uni_service_screen.dart';

class StackScreenUniService extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        HamburgerMenu(),
        UniServiceScreen(),
      ],
      ),
    );
  }
}
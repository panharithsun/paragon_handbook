

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MembershipPackage extends StatelessWidget {
  const MembershipPackage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Membership package:',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(height: 10.0,),
              Container(
                width: MediaQuery.of(context).size.width * 0.49,
                height: 5.0,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0)
                ),
              ),
            ],
          )
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffEFEFEF),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '1 MONTH Subscription',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0,
                      color: Color(0xff192f59)
                    )
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                    child: Center(
                      child: Text(
                        '\$10',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w800,
                          fontSize: 20.0
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20.0),
              Text(
                '\$1 voucher for printing and photocopy service',
                style: TextStyle(
                  fontSize: 18.0
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '6 MONTHS Subscription',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0,
                      color: Color(0xff192f59)
                    )
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                    child: Center(
                      child: Text(
                        '\$50',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w800,
                          fontSize: 20.0
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20.0),
              Text(
                '\$5 voucher for printing and photocopy service',
                style: TextStyle(
                  fontSize: 18.0
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width,
          color: Color(0xffEFEFEF),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '1 YEAR Subscription',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0,
                      color: Color(0xff192f59)
                    )
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                    child: Center(
                      child: Text(
                        '\$80',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w800,
                          fontSize: 20.0
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20.0),
              Text(
                '\$10 voucher for printing and photocopy service',
                style: TextStyle(
                  fontSize: 18.0
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:paragon_handbook/components/bullet_point.dart';

class HowToBecomeAMember extends StatelessWidget {
  const HowToBecomeAMember({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
      color: Color(0xffEFEFEF),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'How to become a member:',
            style: TextStyle(
              color: Color(0xff192f59),
              fontSize: 18.0,
              fontWeight: FontWeight.w600
            ),
          ),
          SizedBox(height: 20.0,),
          BulletPoints(
            text: 'fill in the application form and choose a specific membership package',
          ),
          SizedBox(height: 10.0,),
          BulletPoints(
            text: 'submit the application form to the librarian',
          ),
          SizedBox(height: 10.0,),
          BulletPoints(
            text: 'pay the membership fee and make the deposit at the financial office',
          ),
          SizedBox(height: 10.0,),
          BulletPoints(
            text: 'one or two days after registering for membership, the applicant can come get his/her library member card from the librarian',
          ),
        ],
      )
    );
  }
}
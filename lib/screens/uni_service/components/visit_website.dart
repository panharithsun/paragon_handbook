import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VisitWebsite extends StatelessWidget {
  const VisitWebsite({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Visit the Paragon International University Website',
                  style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.52,
                  height: 5.0,
                  decoration: BoxDecoration(
                      color: Color(0xffFFAE00),
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ],
            )),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
          color: Color(0xffEFEFEF),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'A website of Paragon International University Library (open to the public):',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                height: 60.0,
                padding: EdgeInsets.symmetric(horizontal: 12.0),
                width: 170,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Center(
                  child: Row(
                    children: [
                      Text(
                        'Open Library',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Icon(
                        Icons.open_in_new,
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                'A website of Paragon International University Library (open to Paragon.U students):',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                height: 60.0,
                padding: EdgeInsets.symmetric(horizontal: 12.0),
                width: 210,
                decoration: BoxDecoration(
                  color: Color(0xffFFAE00),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Center(
                  child: Row(
                    children: [
                      Text(
                        'Paragon.U Library',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Icon(
                        Icons.open_in_new,
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
          child: Column(
            children: [
              Text(
                'If you have difficulties accessing your library account, or online resources, please contact the library management at library@paragoniu.edu.kh',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        )
      ],
    );
  }
}

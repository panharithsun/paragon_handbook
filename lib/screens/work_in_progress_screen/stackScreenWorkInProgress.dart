import 'package:flutter/material.dart';
import 'package:paragon_handbook/components/hamburgerMenu.dart';
import 'package:paragon_handbook/screens/work_in_progress_screen/WorkInProgress.dart';

class StackScreenWorkInProgress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          HamburgerMenu(),
          WorkInProgressScreen(),
        ],
      ),
    );
  }
}

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';

class WorkInProgressScreen extends StatefulWidget {

  @override
  _WorkInProgressScreenState createState() => _WorkInProgressScreenState();
}

class _WorkInProgressScreenState extends State<WorkInProgressScreen> {
  double xOffset = 0;

  double yOffset = 0;

  double scaleFactor = 1;

  bool isDrawerOpen = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(xOffset, yOffset, 0)
        ..scale(scaleFactor),
      duration: Duration(milliseconds: 300),
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          toolbarHeight: 60,
          backgroundColor: Color(0xffffffff),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              isDrawerOpen
                  ? IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Color(0xff192f59),
                      ),
                      onPressed: () {
                        setState(() {
                          xOffset = 0;
                          yOffset = 0;
                          scaleFactor = 1;
                          isDrawerOpen = false;
                        });
                      },
                    )
                  : Container(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            xOffset = 200;
                            yOffset = 100;
                            scaleFactor = 0.7;
                            isDrawerOpen = true;
                          });
                        },
                        child: SvgPicture.asset(
                          'assets/svgs/menu.svg',
                          height: 15.0,
                          color: Color(0xff192f59),
                        ),
                      ),
                    ),
              SizedBox(
                width: 25.0,
              ),
              Image.asset(
                'assets/images/logo_with_name_blue.png',
                height: 40.0,
              )
            ],
          ),
        ),
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(20, 60, 40, 10),
              height: 250.0,
              child: Lottie.asset('assets/lotties/work_in_progress.json')
            ),
            Center(
              child: TyperAnimatedTextKit(
                text: ['Work In Progress'],
                textStyle: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w300
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}